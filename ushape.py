import Sketcher
import FreeCAD
import FreeCAD,FreeCADGui,Part, Draft
import os

__title__="FreeCAD Stick Framers Toolkit"
__author__ = "Paul Randall"
__url = "http://www.mathcodeprint.com/"
__command_name__ = "UShape" #Name of the command to appear in Toolbar
__command_group__= "" #Name of Toolbar to assign the command

class UShape_Command:
	def GetResources(slf):
		#print 'Run getResources() for UShape_Command' 
		image_path = '/sketchershapes/icons/U-Shape.png' 
		global_path = FreeCAD.getHomePath()+'Mod' 
		user_path = FreeCAD.getUserAppDataDir()+'Mod' 
		icon_path = '' 

		if os.path.exists(user_path + image_path): 
			icon_path = user_path + image_path 
		elif os.path.exists(global_path + image_path): 
			icon_path = global_path + image_path 
		return {'MenuText': 'UShape', 
			'ToolTip': 'Tooltip for UShape command', 
			'Pixmap' : str(icon_path) }  

	def IsActive(self): 
		if FreeCAD.ActiveDocument == None: 
			#print 'UShape command is NOT active' 
			return False 
		else: 
			#print 'UShape command IS active' 
			return True 
 
	def Activated(self): 
 
		#print 'UShapeCommand activated' 
 
		b=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','UShape') 
		newsampleobject = UShape(b) 
		b.ViewObject.Proxy=0 
		FreeCAD.ActiveDocument.recompute()  
class UShape: 
 
 
	def __init__(self, obj):
 
		#print 'The UShape class has been instantiated init' 
		obj.Proxy = self
		App = FreeCAD
		tmpCircle = None
		geometryList = [] 
		constructionList = [] 
		constraintList = [] 
 
		tmpCircle = Part.Circle ( App.Vector (-1.08420217248609e-19, 0.0, 0.0), App.Vector (0.0, 0.0, 1.0), 25.4)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 0.0, 3.141592653589793))
		geometryList.append( Part.LineSegment( App.Vector (-25.4, 3.110602869834277e-15, 0.0), App.Vector (-25.4, -76.2, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (25.400000000000002, 0.0, 0.0), App.Vector (25.400000000000002, -76.2, 0.0)))


		constraintList.append( Sketcher.Constraint('Coincident',0,3,-1,1 ) )
		constraintList.append( Sketcher.Constraint('PointOnObject',0,2,-1 ) )
		constraintList.append( Sketcher.Constraint('PointOnObject',0,1,-1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',1,1,0,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',2,1,0,1 ) )
		constraintList.append( Sketcher.Constraint('Vertical',2 ) )
		constraintList.append( Sketcher.Constraint('Radius',0,25.4 ))
		constraintList.append( Sketcher.Constraint('Equal',2,1))
		constraintList.append( Sketcher.Constraint('Vertical',1 ) )
		constraintList.append( Sketcher.Constraint('DistanceY',1,2,1,1,76.2))


		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))
		#FreeCAD.Console.print.writeMessage('Changed property: ' + name + 'to ' + newvalue + '') 

	def execute(self,fp):	
		fp.recompute()
		print (' UShape Class executed() ') 
FreeCADGui.addCommand('UShape',UShape_Command() ) 
