import Sketcher
import FreeCAD
import FreeCAD,FreeCADGui,Part
import os
class Trapezoid_Command:
	def GetResources(slf):
		image_path = '/sketchershapes/icons/trapezoid.png' 
		global_path = FreeCAD.getHomePath()+'Mod' 
		user_path = FreeCAD.getUserAppDataDir()+'Mod' 
		icon_path = '' 

		if os.path.exists(user_path + image_path): 
			icon_path = user_path + image_path 
		elif os.path.exists(global_path + image_path): 
			icon_path = global_path + image_path 
		return {'MenuText': 'Trapezoid', 
			'ToolTip': 'Create a sketch with a Trapezoid shape.', 
			'Pixmap' : str(icon_path) }  

	def IsActive(self): 
		if FreeCAD.ActiveDocument == None: 
			return False 
		else: 
			return True 
 
	def Activated(self): 
		b=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','Trapezoid') 
		newsampleobject = Trapezoid(b) 
		b.ViewObject.Proxy=0 
		FreeCAD.ActiveDocument.recompute()  
class Trapezoid: 
 
 
	def __init__(self, obj): 
 
		obj.Proxy = self 
		App = FreeCAD
		geometryList = [] 
		constructionList = [] 
		constraintList = [] 


		geometryList.append( Part.LineSegment( App.Vector (-56.79029996311205, 38.946370977042186, 0.0), App.Vector (-96.12140212446145, -63.47483704831672, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (52.800697, 58.46769, 0.0), App.Vector (125.07879692105452, -24.072698431375862, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-96.12140212446145, -63.47483704831672, 0.0), App.Vector (125.07879692105452, -24.072698431375862, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-56.79029996311205, 38.946370977042186, 0.0), App.Vector (52.800697, 58.46769, 0.0)))
		constructionList.append( Part.LineSegment( App.Vector (-1.9948014815560247, 48.70703048852109, 0.0), App.Vector (14.478697398296536, -43.773767739846285, 0.0)))


		constraintList.append( Sketcher.Constraint('Coincident',2,1,0,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',2,2,1,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',3,1,0,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',3,2,1,1 ) )
		constraintList.append( Sketcher.Constraint('PointOnObject',4,1,3 ) )
		constraintList.append( Sketcher.Constraint('PointOnObject',4,2,2 ) )
		constraintList.append( Sketcher.Constraint('Symmetric',1,1,0,1,4))
		constraintList.append( Sketcher.Constraint('Symmetric',0,2,1,2,4))


		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

		FreeCADGui.SendMsgToActiveView("ViewFit")	


	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))

	def execute(self,fp):	

		fp.recompute()

FreeCADGui.addCommand('Trapezoid', Trapezoid_Command() ) 

