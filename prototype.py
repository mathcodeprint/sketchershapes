import FreeCAD,FreeCADGui,Part, Sketcher
import os

class MYCOMMAND_Command:

	def GetResources(self):
		print "Run getResources() for COMMANDNAMEHERE_command"
		image_path = '/sketchershapes/icons/FILENAMEHERE.png'
		global_path = FreeCAD.getHomePath()+"Mod"
		user_path = FreeCAD.getUserAppDataDir()+"Mod"
		icon_path = ""
		 
		if os.path.exists(user_path + image_path):
			icon_path = user_path + image_path
		elif os.path.exists(global_path + image_path):
			icon_path = global_path + image_path
		return {"MenuText": "MYCOMMAND",
			"ToolTip": "Tooltip for MYCOMMAND command",
			'Pixmap' : str(icon_path) } 

	def IsActive(self):
		if FreeCAD.ActiveDocument == None:
			#print "MYCOMMAND command is NOT active"
			return False
		else:
			#print "MYCOMMAND command IS active"
			return True

	def Activated(self):
		print "MYCOMMAND command activated"

		c=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','MYCOMMAND')
		sketcherObject = MYCOMMAND(c)
		c.ViewObject.Proxy=0
		FreeCAD.ActiveDocument.recompute()

class MYCOMMAND:


	def __init__(self, obj):

		print "The MYCOMMAND class has been instantiated init"
	        obj.Proxy = self

		App = FreeCAD
		geometryList = []
		constructionList = []
		constraintList = []
		
		#Add Geometries

		Lines = obj.addGeometry( geometryList, False)

		#Add Construction Lines

		cLines = obj.addGeometry( constructionList, True)

		#Add Constraints		

		obj.addConstraint( constraintList )

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))
		#FreeCAD.Console.PrintMessage("Changed the property: " + name + " TO: " + newvalue + "\n")
		#print "Property Changed"	

	def execute(self,fp):
		
		print "MYCOMMAND executed() "
			
		
		fp.recompute()

FreeCADGui.addCommand('MYCOMMAND', MYCOMMAND_Command())
