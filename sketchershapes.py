################################
## Utility methods           ###
################################
def isItemSelected():
    selection = FreeCADGui.Selection.getSelectionEx()
    if selection:
        return True
    return False


def getSelectedObject():
    selection = FreeCADGui.Selection.getSelectionEx()
    object = selection[0].Object
    return object


def getSelectedElement():
    selection = FreeCADGui.Selection.getSelectionEx()
    element = selection[0].SubObjects[0]

    return element



