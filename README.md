# sketchershapes

A simple workbench for FreeCAD. This workbench contains a number of constrained sketches for common shapes. This workbench came from an effort to learn how to code FreeCAD Workbenches and Python.  In short, it is probably terrible code.

**Installation**

This Workbench is not available from the Installer in FreeCAD, you must place the files in your Mod directory.

    For Linux:

    /home/username/.FreeCAD/Mod/ 

    For Windows:

    C:\Program Files\FreeCAD\Mod\

    For Mac:

    /Applications/FreeCAD/Mod/


For more details on installing a custom workbench:

    https://wiki.freecadweb.org/Installing_more_workbenches
