import Sketcher
import FreeCAD
import FreeCAD,FreeCADGui,Part, Draft
import os

__title__="FreeCAD Stick Framers Toolkit"
__author__ = "Paul Randall"
__url = "http://www.mathcodeprint.com/"
__command_name__ = "Corner" #Name of the command to appear in Toolbar
__command_group__= "" #Name of Toolbar to assign the command

class Corner_Command:
	def GetResources(slf):
		#print 'Run getResources() for Corner_Command' 
		image_path = '/sketchershapes/icons/corner.png' 
		global_path = FreeCAD.getHomePath()+'Mod' 
		user_path = FreeCAD.getUserAppDataDir()+'Mod' 
		icon_path = '' 

		if os.path.exists(user_path + image_path): 
			icon_path = user_path + image_path 
		elif os.path.exists(global_path + image_path): 
			icon_path = global_path + image_path 
		return {'MenuText': 'Corner', 
			'ToolTip': 'Tooltip for Corner command', 
			'Pixmap' : str(icon_path) }  

	def IsActive(self): 
		if FreeCAD.ActiveDocument == None: 
			#print 'Corner command is NOT active' 
			return False 
		else: 
			#print 'Corner command IS active' 
			return True 
 
	def Activated(self): 
 
		#print 'CornerCommand activated' 
 
		b=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','Corner') 
		newsampleobject = Corner(b) 
		b.ViewObject.Proxy=0 
		FreeCAD.ActiveDocument.recompute()  
class Corner: 
 
 
	def __init__(self, obj):
 
		#print 'The Corner class has been instantiated init' 
		obj.Proxy = self
		App = FreeCAD
		tmpCircle = None
		geometryList = [] 
		constructionList = [] 
		constraintList = [] 
 
		geometryList.append( Part.LineSegment( App.Vector (-6.555406499999999, 40.66641698141504, 0.0), App.Vector (6.555406499999999, 40.66641698141504, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (6.555406499999999, 40.66641698141504, 0.0), App.Vector (6.555406499999999, 6.555406499999997, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (6.555406499999999, 6.555406499999998, 0.0), App.Vector (40.66641698141504, 6.555406499999998, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (40.66641698141504, 6.555406499999998, 0.0), App.Vector (40.66641698141504, -6.555406499999998, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (40.66641698141504, -6.555406499999998, 0.0), App.Vector (-6.555406499999997, -6.555406499999998, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-6.555406499999997, -6.555406499999998, 0.0), App.Vector (-6.555406499999999, 40.66641698141504, 0.0)))


		constraintList.append( Sketcher.Constraint('Coincident',0,2,1,1 ) )
		constraintList.append( Sketcher.Constraint('Vertical',1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',1,2,2,1 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',2,2,3,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',3,2,4,1 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',4 ) )
		constraintList.append( Sketcher.Constraint('Coincident',4,2,5,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',5,2,0,1 ) )
		constraintList.append( Sketcher.Constraint('Equal',0,3))
		constraintList.append( Sketcher.Constraint('Equal',1,2))
		constraintList.append( Sketcher.Constraint('Equal',5,4))
		constraintList.append( Sketcher.Constraint('Symmetric',0,2,0,1,-2))
		constraintList.append( Sketcher.Constraint('Symmetric',2,2,3,2,-1))


		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))
		#FreeCAD.Console.print.writeMessage('Changed property: ' + name + 'to ' + newvalue + '') 

	def execute(self,fp):	
		fp.recompute()
		print (' Corner Class executed() ') 
FreeCADGui.addCommand('Corner',Corner_Command() ) 
