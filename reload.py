import FreeCAD,FreeCADGui,Part
import os

class MyReload_Command:

	def GetResources(self):
		image_path = '/pacman/icons/MyReload.png'
		global_path = FreeCAD.getHomePath()+"Mod"
		user_path = FreeCAD.getUserAppDataDir()+"Mod"
		icon_path = ""
		 
		if os.path.exists(user_path + image_path):
			icon_path = user_path + image_path
		elif os.path.exists(global_path + image_path):
			icon_path = global_path + image_path
		return {"MenuText": "MyReload",
			"ToolTip": "Reload my classes",
			'Pixmap' : str(icon_path) } 

	def IsActive(self):
		if FreeCAD.ActiveDocument == None:
			return False
		else:
			return True

	def Activated(self):
		import pacman
		reload(pacman)



FreeCADGui.addCommand('MyReload', MyReload_Command())


