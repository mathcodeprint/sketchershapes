import FreeCAD,FreeCADGui,Part, Sketcher
import os

class Plus_Command:

	def GetResources(self):
		image_path = '/sketchershapes/icons/plus.svg'
		global_path = FreeCAD.getHomePath()+"Mod"
		user_path = FreeCAD.getUserAppDataDir()+"Mod"
		icon_path = ""
		 
		if os.path.exists(user_path + image_path):
			icon_path = user_path + image_path
		elif os.path.exists(global_path + image_path):
			icon_path = global_path + image_path
		return {"MenuText": "plus Menu",
			"ToolTip": "Create a sketch of a plus symbol",
			'Pixmap' : str(icon_path) } 

	def IsActive(self):
		if FreeCAD.ActiveDocument == None:
			return False
		else:
			return True

	def Activated(self):
		b=FreeCAD.ActiveDocument.addObject("Sketcher::SketchObjectPython","Plus")
		newPlus = Plus(b)
		b.ViewObject.Proxy=0
		FreeCAD.ActiveDocument.recompute() 

class Plus:

	def __init__(self, obj):

		obj.Proxy = self

		App = FreeCAD
		geometryList = []
		constructionList = []
		constraintList = []

		geometryList.append( Part.LineSegment( App.Vector (-11.349931000000002, 34.04979300000001, 0.0), App.Vector (11.349931000000002, 34.04979300000001, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (11.349931000000002, 34.04979300000001, 0.0), App.Vector (11.349931000000002, 11.349931000000002, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (11.349931000000002, 11.349931000000002, 0.0), App.Vector (34.04979300000001, 11.349931000000002, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (34.04979300000001, 11.349931000000002, 0.0), App.Vector (34.04979300000001, -11.349931000000002, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (34.04979300000001, -11.349931000000002, 0.0), App.Vector (11.349931000000005, -11.349931000000002, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (11.349931000000005, -11.349931000000002, 0.0), App.Vector (11.349931000000003, -34.04979300000001, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (11.349931000000003, -34.04979300000001, 0.0), App.Vector (-11.349931, -34.04979300000001, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-11.349931, -34.04979300000001, 0.0), App.Vector (-11.349931, -11.349931000000002, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-11.349931, -11.349931000000002, 0.0), App.Vector (-34.049793, -11.349931000000002, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-34.049793, -11.349931000000002, 0.0), App.Vector (-34.049793, 11.349931000000002, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-34.049793, 11.349931000000002, 0.0), App.Vector (-11.349931000000002, 11.349931000000002, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-11.349931000000002, 11.349931000000002, 0.0), App.Vector (-11.349931000000002, 34.04979300000001, 0.0)))

		constraintList.append( Sketcher.Constraint('Coincident',1,1,0,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',2,1,1,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',3,1,2,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',4,1,3,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',5,1,4,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',6,1,5,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',7,1,6,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',8,1,7,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',9,1,8,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',10,1,9,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',11,1,10,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',11,2,0,1 ) )
		constraintList.append( Sketcher.Constraint('Equal',10,11))
		constraintList.append( Sketcher.Constraint('Equal',11,0))
		constraintList.append( Sketcher.Constraint('Equal',0,1))
		constraintList.append( Sketcher.Constraint('Equal',1,2))
		constraintList.append( Sketcher.Constraint('Equal',2,3))
		constraintList.append( Sketcher.Constraint('Equal',3,4))
		constraintList.append( Sketcher.Constraint('Equal',4,5))
		constraintList.append( Sketcher.Constraint('Equal',5,6))
		constraintList.append( Sketcher.Constraint('Equal',6,7))
		constraintList.append( Sketcher.Constraint('Equal',7,8))
		constraintList.append( Sketcher.Constraint('Equal',8,9))

		
		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

		FreeCADGui.SendMsgToActiveView("ViewFit")	

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))

	
	def execute(self,fp):	
		fp.recompute()

			

FreeCADGui.addCommand('Plus', Plus_Command())

