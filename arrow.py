import FreeCAD,FreeCADGui,Part, Sketcher
import os

class Arrow_Command:

	def GetResources(self):

		#Check the modules directory for the icon.
		image_path = '/sketchershapes/icons/arrow.png'
		global_path = FreeCAD.getHomePath()+"Mod"
		user_path = FreeCAD.getUserAppDataDir()+"Mod"
		icon_path = ""

		#Check a couple of other places for an icon.		 
		if os.path.exists(user_path + image_path):
			icon_path = user_path + image_path
		elif os.path.exists(global_path + image_path):
			icon_path = global_path + image_path
		return {"MenuText": "Arrow Menu",
			"ToolTip": "Draws a sketch with an arrow.",
			'Pixmap' : str(icon_path) } 

	def IsActive(self):
		if FreeCAD.ActiveDocument == None:
			return False
		else:
			return True

	def Activated(self):
		b=FreeCAD.ActiveDocument.addObject("Sketcher::SketchObjectPython","Arrow")
		newarrowobject = Arrow(b)
		b.ViewObject.Proxy=0
		FreeCAD.ActiveDocument.recompute() 

class Arrow:


	def __init__(self, obj):

		obj.Proxy = self

		App = FreeCAD
		geometryList = []

		constraintList = []

		geometryList.append( Part.LineSegment(App.Vector(0,10,0),App.Vector(4,7,0)) )
		geometryList.append( Part.LineSegment(App.Vector(4,7,0),App.Vector(2,7,0)) )
		geometryList.append( Part.LineSegment(App.Vector(2,7,0),App.Vector(2,0,0)) )
		geometryList.append( Part.LineSegment(App.Vector(2,0,0),App.Vector(-2,0,0)) )
		geometryList.append( Part.LineSegment(App.Vector(-2,0,0),App.Vector(-2,7,0)) )
		geometryList.append( Part.LineSegment(App.Vector(-2,7,0),App.Vector(-4,7,0)) )
		geometryList.append( Part.LineSegment(App.Vector(-4,7,0),App.Vector(0,10,0)) )

		constraintList.append(Sketcher.Constraint('Coincident',0,2,1,1)) 
		constraintList.append(Sketcher.Constraint('Coincident',1,2,2,1)) 
		constraintList.append(Sketcher.Constraint('Coincident',2,2,3,1)) 
		constraintList.append(Sketcher.Constraint('Coincident',3,2,4,1)) 
		constraintList.append(Sketcher.Constraint('Coincident',4,2,5,1)) 
		constraintList.append(Sketcher.Constraint('Coincident',5,2,6,1)) 
		constraintList.append(Sketcher.Constraint('Coincident',6,2,0,1)) 

		constraintList.append(Sketcher.Constraint('Equal',2,4)) 
		constraintList.append(Sketcher.Constraint('Equal',0,6))
		constraintList.append(Sketcher.Constraint('Equal',1,5)) 
		constraintList.append(Sketcher.Constraint('Equal',5,1)) 

		constraintList.append(Sketcher.Constraint('Perpendicular',1,2)) 
		constraintList.append(Sketcher.Constraint('Perpendicular',2,3)) 
		constraintList.append(Sketcher.Constraint('Perpendicular',3,4)) 
		constraintList.append(Sketcher.Constraint('Perpendicular',4,5)) 

		obj.addGeometry( geometryList, False)
		obj.addConstraint( constraintList )




	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))
	
	def execute(self,fp):	
		FreeCADGui.SendMsgToActiveView("ViewFit")	
		fp.recompute()
			

FreeCADGui.addCommand('Arrow', Arrow_Command())

