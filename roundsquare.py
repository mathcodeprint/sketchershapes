import FreeCAD,FreeCADGui,Part, Sketcher
import os

class RoundedSquare_Command:

	def GetResources(self):
		print "Run getResources() for RoundedSquare_command"
		image_path = '/sketchershapes/icons/roundedsquare.png'
		global_path = FreeCAD.getHomePath()+"Mod"
		user_path = FreeCAD.getUserAppDataDir()+"Mod"
		icon_path = ""
		 
		if os.path.exists(user_path + image_path):
			icon_path = user_path + image_path
		elif os.path.exists(global_path + image_path):
			icon_path = global_path + image_path
		return {"MenuText": "Rounded Square",
			"ToolTip": "Tooltip for Rounded Square command",
			'Pixmap' : str(icon_path) } 

	def IsActive(self):
		if FreeCAD.ActiveDocument == None:
			#print "RoundedSquare command is NOT active"
			return False
		else:
			#print "RoundedSquare command IS active"
			return True

	def Activated(self):
		print "Rounded Square command activated"

		c=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','RoundedSquare')
		sketcherObject = RoundSquare(c)
		c.ViewObject.Proxy=0
		FreeCAD.ActiveDocument.recompute()

class RoundedSquare:


	def __init__(self, obj):

		print "The RoundedSquare class has been instantiated init"
	        obj.Proxy = self

		App = FreeCAD
		geometryList = []
		constructionList = []
		constraintList = []

		#Lines and Circles

		geometryList.append(Part.LineSegment(App.Vector(-8.243449,6.452739,0),App.Vector(8.181702,6.452739,0)))
		geometryList.append(Part.LineSegment(App.Vector(8.181702,6.452739,0),App.Vector(8.181702,-7.502466,0)))
		geometryList.append(Part.LineSegment(App.Vector(8.181702,-7.502466,0),App.Vector(-8.243449,-7.502466,0)))
		geometryList.append(Part.LineSegment(App.Vector(-8.243449,-7.502466,0),App.Vector(-8.243449,6.452739,0)))

		Lines = obj.addGeometry( geometryList, False)

		#Construction Lines

		cLines = obj.addGeometry( constructionList, True)

		#constraints		
		constraintList.append(Sketcher.Constraint('Coincident',0,2,1,1))
		constraintList.append(Sketcher.Constraint('Coincident',1,2,2,1))
		constraintList.append(Sketcher.Constraint('Coincident',2,2,3,1))
		constraintList.append(Sketcher.Constraint('Coincident',3,2,0,1))
		constraintList.append(Sketcher.Constraint('Horizontal',0))
		constraintList.append(Sketcher.Constraint('Horizontal',2))
		constraintList.append(Sketcher.Constraint('Vertical',1))
		constraintList.append(Sketcher.Constraint('Vertical',3))
		constraintList.append(Sketcher.Constraint('Symmetric',0,1,0,2,-2)) 
		constraintList.append(Sketcher.Constraint('Symmetric',0,2,1,2,-1)) 

		#constraintList.append(Sketcher.Constraint('DistanceX',0,1,0,2,16.363404)
		#constraintList.append(Sketcher.Constraint('DistanceY',1,2,1,1,14.439247)) 

		#App.ActiveDocument.Sketch.setDatum(10,App.Units.Quantity('10.000000 mm'))
		#App.ActiveDocument.Sketch.setDatum(11,App.Units.Quantity('10.000000 mm'))

		named_constriant.Name = "Distance to Center"
		constraintList.append( named_constriant )

		#App.ActiveDocument.Sketch.fillet(0,1,4)
		#App.ActiveDocument.Sketch.fillet(1,2,4)
		#App.ActiveDocument.Sketch.fillet(2,3,4)
		#App.ActiveDocument.Sketch.fillet(3,0,4)

		obj.addConstraint( constraintList )

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))
		#FreeCAD.Console.PrintMessage("Changed the property: " + name + " TO: " + newvalue + "\n")
		#print "Property Changed"	

	def execute(self,fp):
		
		print "RoundedSquare executed() "
			
		
		fp.recompute()

FreeCADGui.addCommand('RoundedSquare', RoundedSquare_Command())

