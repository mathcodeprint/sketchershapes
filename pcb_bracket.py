import Sketcher
import FreeCAD
import FreeCAD,FreeCADGui,Part
import os
class BoardBrackets_Command:
	def GetResources(slf):
		image_path = '/sketchershapes/icons/PCBBrackets.png' 
		global_path = FreeCAD.getHomePath()+'Mod' 
		user_path = FreeCAD.getUserAppDataDir()+'Mod' 
		icon_path = '' 

		if os.path.exists(user_path + image_path): 
			icon_path = user_path + image_path 
		elif os.path.exists(global_path + image_path): 
			icon_path = global_path + image_path 
		return {'MenuText': 'BoardBrackets Menu', 
			'ToolTip': 'Create a sketch for PCB Brackets', 
			'Pixmap' : str(icon_path) }  

	def IsActive(self): 
		if FreeCAD.ActiveDocument == None: 
			#print 'BoardBrackets command is NOT active' 
			return False 
		else: 
			#print 'BoardBrackets command IS active' 
			return True 
 
	def Activated(self): 
 
		b=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','BoardBrackets') 
		newsampleobject = BoardBrackets(b) 
		b.ViewObject.Proxy=0 
		FreeCAD.ActiveDocument.recompute()  
class BoardBrackets: 
 
 
	def __init__(self, obj): 
 
		#print 'The BoardBracket class has been instantiated init' 

		obj.Proxy = self 
		App = FreeCAD
		geometryList = [] 
		constructionList = [] 
		constraintList = [] 
 
		geometryList.append( Part.LineSegment( App.Vector (-13.681259710024463, 0.875, 0.0), App.Vector (-13.681259710024463, -0.875, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-13.681259710024463, -0.875, 0.0), App.Vector (-11.181259710024467, -0.875, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-11.181259710024467, -0.875, 0.0), App.Vector (-11.181259710024467, -2.625, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-11.181259710024467, -2.625, 0.0), App.Vector (-16.181259710024467, -2.625, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-16.181259710024467, -2.625, 0.0), App.Vector (-16.181259710024467, 2.625, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-16.181259710024467, 2.625, 0.0), App.Vector (-11.181259710024467, 2.625, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-11.181259710024467, 2.625, 0.0), App.Vector (-11.181259710024467, 0.875, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-11.181259710024467, 0.875, 0.0), App.Vector (-13.681259710024463, 0.875, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (13.681259710024463, 0.875, 0.0), App.Vector (13.681259710024463, -0.875, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (13.681259710024463, -0.875, 0.0), App.Vector (11.181259710024467, -0.875, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (11.181259710024467, -0.875, 0.0), App.Vector (11.181259710024467, -2.625, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (11.181259710024467, -2.625, 0.0), App.Vector (16.181259710024467, -2.625, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (16.181259710024467, -2.625, 0.0), App.Vector (16.181259710024467, 2.625, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (16.181259710024467, 2.625, 0.0), App.Vector (11.181259710024465, 2.625, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (11.181259710024465, 2.625, 0.0), App.Vector (11.181259710024467, 0.875, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (11.181259710024467, 0.875, 0.0), App.Vector (13.681259710024463, 0.875, 0.0)))


		constraintList.append( Sketcher.Constraint('Vertical',0 ) )
		constraintList.append( Sketcher.Constraint('Coincident',1,1,0,2 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',2,1,1,2 ) )
		constraintList.append( Sketcher.Constraint('Vertical',2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',3,1,2,2 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',3 ) )
		constraintList.append( Sketcher.Constraint('Coincident',4,1,3,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',5,1,4,2 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',5 ) )
		constraintList.append( Sketcher.Constraint('Coincident',6,1,5,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',7,1,6,2 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',7 ) )
		constraintList.append( Sketcher.Constraint('Coincident',0,1,7,2 ) )
		constraintList.append( Sketcher.Constraint('Symmetric',4,2,3,2,-1))
		constraintList.append( Sketcher.Constraint('Equal',5,3))
		constraintList.append( Sketcher.Constraint('Equal',6,2))
		constraintList.append( Sketcher.Constraint('Equal',7,1))
		constraintList.append( Sketcher.Constraint('Distance',6,2,1,2,1.75))
		constraintList.append( Sketcher.Constraint('Vertical',8 ) )
		constraintList.append( Sketcher.Constraint('Coincident',9,1,8,2 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',9 ) )
		constraintList.append( Sketcher.Constraint('Coincident',10,1,9,2 ) )
		constraintList.append( Sketcher.Constraint('Vertical',10 ) )
		constraintList.append( Sketcher.Constraint('Coincident',11,1,10,2 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',11 ) )
		constraintList.append( Sketcher.Constraint('Coincident',12,1,11,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',13,1,12,2 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',13 ) )
		constraintList.append( Sketcher.Constraint('Coincident',14,1,13,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',15,1,14,2 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',15 ) )
		constraintList.append( Sketcher.Constraint('Coincident',8,1,15,2 ) )
		constraintList.append( Sketcher.Constraint('Equal',13,11))
		constraintList.append( Sketcher.Constraint('Equal',14,10))
		constraintList.append( Sketcher.Constraint('Equal',15,9))
		constraintList.append( Sketcher.Constraint('Equal',6,14))
		constraintList.append( Sketcher.Constraint('Symmetric',12,2,11,2,-1))
		constraintList.append( Sketcher.Constraint('Symmetric',5,2,13,2,-2))
		constraintList.append( Sketcher.Constraint('Distance',4,0,-2000,0,5.25))
		constraintList.append( Sketcher.Constraint('Distance',5,0,-2000,0,5.0))
		constraintList.append( Sketcher.Constraint('Distance',0,1,4,0,2.5))
		constraintList.append( Sketcher.Constraint('Equal',13,5))
		constraintList.append( Sketcher.Constraint('Equal',15,7))


		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

		FreeCADGui.SendMsgToActiveView("ViewFit")	

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))

	def execute(self,fp):	
		fp.recompute()
FreeCADGui.addCommand('BoardBrackets',BoardBrackets_Command() ) 

