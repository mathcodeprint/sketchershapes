import FreeCAD,FreeCADGui,Part, Sketcher, Draft, os
import sketchershapes

class Codify_Command:

	def GetResources(self):

		#Check the modules directory for the icon.
		image_path = '/sketchershapes/icons/codify.png'
		global_path = FreeCAD.getHomePath()+"Mod"
		user_path = FreeCAD.getUserAppDataDir()+"Mod"
		icon_path = ""

		#Check a couple of other places for an icon.		 
		if os.path.exists(user_path + image_path):
			icon_path = user_path + image_path
		elif os.path.exists(global_path + image_path):
			icon_path = global_path + image_path
		return {"MenuText": "Codify",
			"ToolTip": "Creates a Workbench Command File for Hightlighted Sketch",
			'Pixmap' : str(icon_path) } 

	def IsActive(self):
		if FreeCAD.ActiveDocument == None:
			return False
		else:
			return True

	def Activated(self):

		#
		#	Change to work on selected sketch, not the entire document
		#

		#Change to be selected object
		selection = FreeCADGui.Selection.getCompleteSelection()
		#for object in FreeCAD.ActiveDocument.Objects:

		#TODO: add no selection and multiple selection warnings.
		if (selection[0].TypeId == 'Sketcher::SketchObject' or selection[0].TypeId == 'Sketcher::SketchObjectPython'):
			print ( selection[0].TypeId )
			newcode = Codify( selection[0] )
			newcode.codifySketch()
			

class Codify():

	sketchObject = "";

	def __init__(self, obj):
		self.sketchObject = obj;


	def codifySketch(self):

		sketchname = self.sketchObject.Label

		constraints = self.sketchObject.Constraints
		geometry =self.sketchObject.Geometry

		user_path = FreeCAD.getUserAppDataDir()+"Mod/"
		mod = "sketchershapes"
		cmd = "commands/"
		print ( user_path )
		
		file_name = sketchname.lower()

	# TODO: use command path when dynamic command loading works

	#	file_object  = open( user_path + mod + cmd + "TEST" + file_name + ".py", "w") 

		file_object  = open( user_path + mod + "/TEST" + file_name + ".py", "w") 
		print ( "Saving Sketch code to:"  +	 user_path + mod + "/TEST" + file_name + ".py" )


		file_string = [];

	#	print ( sketchname )
		
		file_string.append ( "import Sketcher" )
		file_string.append ( "import FreeCAD" )
		
		file_string.append ("import FreeCAD,FreeCADGui,Part, Draft")
		file_string.append ("import os")

		file_string.append ("\n__title__=\"FreeCAD Stick Framers Toolkit\"")
		file_string.append ("__author__ = \"Paul Randall\"")
		file_string.append ("__url = \"http://www.mathcodeprint.com/\"")

		file_string.append ("__command_name__ = \"" + sketchname +"\" #Name of the command to appear in Toolbar" )   
		file_string.append ("__command_group__= \"\" #Name of Toolbar to assign the command")

		
		file_string.append ("\nclass " + sketchname +"_Command:")
		
		file_string.append("	def GetResources(slf):")
		file_string.append("		#print 'Run getResources() for " + sketchname +"_Command' ")
	#TODO: swap out hard coded mod path
		file_string.append("		image_path = '/"  + mod + "/icons/" + sketchname.lower() +".png' ")
		file_string.append("		global_path = FreeCAD.getHomePath()+'Mod' ")
		file_string.append("		user_path = FreeCAD.getUserAppDataDir()+'Mod' ")
		file_string.append("		icon_path = '' ")
		file_string.append("")	 
		file_string.append("		if os.path.exists(user_path + image_path): ")
		file_string.append("			icon_path = user_path + image_path ")
		file_string.append("		elif os.path.exists(global_path + image_path): ")
		file_string.append("			icon_path = global_path + image_path ")
		file_string.append("		return {'MenuText': '" + sketchname +"', ")
		file_string.append("			'ToolTip': 'Tooltip for " + sketchname + " command', ")
		file_string.append("			'Pixmap' : str(icon_path) }  ")
		file_string.append("")
		file_string.append("	def IsActive(self): ")
		file_string.append("		if FreeCAD.ActiveDocument == None: ")
		file_string.append("			#print '" + sketchname +" command is NOT active' ")
		file_string.append("			return False ")
		file_string.append("		else: ")
		file_string.append("			#print '" + sketchname +" command IS active' ")
		file_string.append("			return True ")
		file_string.append(" ")
		file_string.append("	def Activated(self): ")
		file_string.append(" ")
		file_string.append("		#print '" + sketchname +"Command activated' ")
		file_string.append(" ")
		file_string.append("		b=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','" + sketchname +"') ")
		file_string.append("		newsampleobject = " + sketchname +"(b) ")
		file_string.append("		b.ViewObject.Proxy=0 ")
		file_string.append("		FreeCAD.ActiveDocument.recompute()  ")


		#
		#	Sets up the sketch class
		#

		file_string.append("class " + sketchname +": ")
		file_string.append(" ")
		file_string.append(" ")
		file_string.append("	def __init__(self, obj):")
		file_string.append(" ")
		file_string.append("		#print 'The " + sketchname +" class has been instantiated init' ")
		file_string.append("		obj.Proxy = self")
		
		file_string.append("		App = FreeCAD")
		
		file_string.append("		tmpCircle = None")
		
		file_string.append("		geometryList = [] ")
		file_string.append("		constructionList = [] ")
		file_string.append("		constraintList = [] ")
		file_string.append(" ")

		#print("		sketch = App.ActiveDocument.Sketch ")
		#print("		constraints = App.ActiveDocument.Sketch.Constraints ")
		#print("		geometry = App.ActiveDocument.Sketch.Geometry  ")


		#
		#	Add all the geometries
		#

		# Construction has been moved to the Geometry List
		# Construction status is not available at the object level anymore
		# maybe iterate geometry by value and use
		# geometry.getConstruction[x] where x is the same as the loop index.
		

		for object in geometry:
#			if not object.Construction:
			if type( object ) == Part.Point:					
				pass

			if type( object ) == Part.LineSegment:		
				file_string.append ("\t\tgeometryList.append( Part.LineSegment( App." + 
											str(object.StartPoint) + ", App." + 
											str(object.EndPoint) + "))" )
			if type( object ) == Part.Circle:		
				file_string.append ("\t\tgeometryList.append( Part.Circle ( App." + 
											str(object.Center) + ", App." + 
											str(object.Axis) +   ", " + 
											str(object.Radius)  + "))" )
			if type( object ) == Part.ArcOfCircle:	
	
				file_string.append( "\t\ttmpCircle = Part.Circle ( App." + 
								str(object.Center) + ", App." + 
								str(object.Axis) +   ", " + 
								str(object.Radius)  + ")" )
	
				file_string.append ("\t\tgeometryList.append( Part.ArcOfCircle (  tmpCircle, " +
											str(object.FirstParameter) +   ", " + 
											str(object.LastParameter)  + "))" )	
			if type ( object ) == Part.ArcOfCircle:
				pass
			if type ( object ) == Part.ArcOfConic:
				pass
			if type ( object ) == Part.ArcOfHyperbola:
				pass
			if type ( object ) == Part.ArcOfParabola:
				pass

			if type ( object ) == Part.BSplineCurve:
				print ("B-Spline - not supported" )

			if type ( object ) == Part.BSplineSurface:
				print ("B-BSplineSurface - not supported" )

			if type ( object ) == Part.BezierCurve:
				print ("BezierCurve - not supported" )

			if type ( object ) == Part.Conic:
				print ("Conice - not supported" )

			if type ( object ) == Part.Cone:
				print ("Cone - not supported" )

			if type ( object ) == Part.Ellipse:
				print ("Ellipse - not supported" )





	
#			if object.Construction:
#				if type( object ) == Part.LineSegment:
#					file_string.append ("\t\tconstructionList.append( Part.LineSegment( App." + 
#												str(object.StartPoint) + ", App." + 
#												str(object.EndPoint) + "))" )		
#				if type( object ) == Part.Circle:		
#					file_string.append  ("\t\tconstructionList.append( Part.Circle ( App." + 
#												str(object.Center) + ", App." + 
#												str(object.Axis) +   ", " + 
#												str(object.Radius)  + "))" )
		file_string.append ("\n")

		#
		# Add all the constraints
		#

		for constraint in constraints:
				
			if constraint.Type == "Coincident":
				file_string.append ( "\t\tconstraintList.append( Sketcher.Constraint('Coincident'," + 
						str(constraint.First) + "," + str(constraint.FirstPos) + "," +
						str(constraint.Second) + "," + str(constraint.SecondPos) + " ) )" )
		
			if constraint.Type == "Horizontal":
				if constraint.Second != -2000:
					file_string.append ( "\t\tconstraintList.append( Sketcher.Constraint('Horizontal'," + 
						str(constraint.First) + "," + str(constraint.FirstPos) + "," +
						str(constraint.Second) + "," + str(constraint.SecondPos) + " ) )" )
				else:
					file_string.append ( "\t\tconstraintList.append( Sketcher.Constraint('Horizontal'," + 
							str(constraint.First) +  " ) )" )
		
			if constraint.Type == "Vertical":
				if constraint.Second != -2000:
					file_string.append ( "\t\tconstraintList.append( Sketcher.Constraint('Vertical'," + 
						str(constraint.First) + "," + str(constraint.FirstPos) + "," +
						str(constraint.Second) + "," + str(constraint.SecondPos) + " ) )" )
				else:
					file_string.append ( "\t\tconstraintList.append( Sketcher.Constraint('Vertical'," + 
							str(constraint.First) + " ) )" )
		
			if constraint.Type == "Equal":
				file_string.append ( "\t\tconstraintList.append( Sketcher.Constraint('Equal'," + 
						str(constraint.First) + ","  + str(constraint.Second) + "))" )
		
			if constraint.Type == "Parallel":
				file_string.append ( "\t\tconstraintList.append( Sketcher.Constraint('Parallel'," + 
						str(constraint.First) + ","  + str(constraint.Second) + "))" )

			if constraint.Type == "Tangent":
				file_string.append ( "\t\tconstraintList.append( Sketcher.Constraint('Tangent'," + 
						str(constraint.First) + "," + str(constraint.FirstPos) + "," +
						str(constraint.Second) + "," + str(constraint.SecondPos) + " ) )" )

			if constraint.Type == "Distance" and constraint.Name == '': 
				file_string.append ( "\t\tconstraintList.append( Sketcher.Constraint('Distance'," +
						str(constraint.First) + "," + str(constraint.FirstPos) + "," +
						str(constraint.Second) + "," + str(constraint.SecondPos) + "," +
						str(constraint.Value) +"))" )
			elif constraint.Type == "Distance" and constraint.Name != "" :
				file_string.append ("\t\tnamed_constraint = Sketcher.Constraint('Distance'," +
						str(constraint.First) + "," + str(constraint.FirstPos) + "," +
						str(constraint.Second) + "," + str(constraint.SecondPos) + "," +
						str(constraint.Value) +")" )
				file_string.append ("\t\tnamed_constraint.Name = \"" + constraint.Name + "\"")
				file_string.append ("\t\tconstraintList.append( named_constraint )")
				pass
		
			if constraint.Type == "DistanceY" and constraint.Name == '': 
				file_string.append ( "\t\tconstraintList.append( Sketcher.Constraint('DistanceY'," +
						str(constraint.First) + "," + str(constraint.FirstPos) + "," +
						str(constraint.Second) + "," + str(constraint.SecondPos) + "," +
						str(constraint.Value) +"))" )
			elif constraint.Type == "DistanceY" and constraint.Name != "" :
				file_string.append ("\t\tnamed_constraint = Sketcher.Constraint('DistanceY'," +
						str(constraint.First) + "," + str(constraint.FirstPos) + "," +
						str(constraint.Second) + "," + str(constraint.SecondPos) + "," +
						str(constraint.Value) +")" )
				file_string.append ("\t\tnamed_constraint.Name = \"" + constraint.Name + "\"")
				file_string.append ("\t\tconstraintList.append( named_constraint )")
				pass
		
			if constraint.Type == "DistanceX" and constraint.Name == '':
				file_string.append ( "\t\tconstraintList.append( Sketcher.Constraint('DistanceX'," +
						str(constraint.First) + "," + str(constraint.FirstPos) + "," +
						str(constraint.Second) + "," + str(constraint.SecondPos) + "," +
						str(constraint.Value) +"))" )
			elif constraint.Type == "DistanceX" and constraint.Name != "" :
				file_string.append ("\t\tnamed_constraint = Sketcher.Constraint('DistanceX'," +
						str(constraint.First) + "," + str(constraint.FirstPos) + "," +
						str(constraint.Second) + "," + str(constraint.SecondPos) + "," +
						str(constraint.Value) +")" )
				file_string.append ("\t\tnamed_constraint.Name = \"" + constraint.Name + "\"")
				file_string.append ("\t\tconstraintList.append( named_constraint )")
				pass
		
			if constraint.Type == "Radius":
				file_string.append ( "\t\tconstraintList.append( Sketcher.Constraint('Radius'," + 
						str(constraint.First) + "," + str(constraint.Value) + " ))" )
		
			if constraint.Type == "Perpendicular":
				file_string.append ( "\t\tconstraintList.append( Sketcher.Constraint('Perpendicular'," + 
						str(constraint.First) + "," + str(constraint.FirstPos) + "," +
						str(constraint.Second)  + " ) )" )
			#PointOnObject
			if constraint.Type == "PointOnObject":
				file_string.append ( "\t\tconstraintList.append( Sketcher.Constraint('PointOnObject'," + 
						str(constraint.First) + "," + str(constraint.FirstPos) + "," +
						str(constraint.Second) + " ) )" )
		#Symetry Line
		#>>> App.ActiveDocument.Sketch.addConstraint(Sketcher.Constraint('Symmetric',0,2,0,1,-1)) 
		#Symetry Point
		# App.ActiveDocument.Sketch.addConstraint(Sketcher.Constraint('Symmetric',0,1,1,1,-1,1)) 
			if constraint.Type == "Symmetric":
				file_string.append ( "\t\tconstraintList.append( Sketcher.Constraint('Symmetric'," + 
						str(constraint.First) + "," + str(constraint.FirstPos) + "," +
						str(constraint.Second) + "," + str(constraint.SecondPos) + "," +
						str(constraint.Third) +"))" ) 
	
		file_string.append ("\n")
		file_string.append ("		obj.addGeometry( geometryList, False)")
		file_string.append ("		obj.addGeometry( constructionList, True)")
		file_string.append ("		obj.addConstraint( constraintList )")

		file_string.append ("\n	def onChanged(self, fp, prop):")
		file_string.append ("		name = str(prop)")
		file_string.append ("		newvalue = str(fp.getPropertyByName(str(prop)))")
		file_string.append ("		#FreeCAD.Console.print.writeMessage('Changed property: ' + name + 'to ' + newvalue + '') ")

		file_string.append ("")
		file_string.append ("	def execute(self,fp):	")
		file_string.append ("		fp.recompute()")
		file_string.append ("		print (' " + sketchname + " Class executed() ') ")
					
		file_string.append ("FreeCADGui.addCommand('" + sketchname +"'," + sketchname + "_Command() ) ")

		#
		#	Write the finished Class to file.
		#

		for string in file_string:
			file_object.write( string + "\n" )
		file_object.close()


	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))
	
	def execute(self,fp):
		FreeCADGui.SendMsgToActiveView("ViewFit")	
		fp.recompute()


FreeCADGui.addCommand('Codify', Codify_Command())

