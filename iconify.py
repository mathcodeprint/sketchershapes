import FreeCAD,FreeCADGui,Part, Sketcher, Draft, os
import sys

sys.path.append("/home/randall/anaconda3/lib/python3.7/site-packages/")

import pysvg

from pysvg import structure
from pysvg import builders
from pysvg import style

class Iconify_Command:

	def GetResources(self):

		#Check the modules directory for the icon.
		image_path = '/sketchershapes/icons/iconify.png'
		global_path = FreeCAD.getHomePath()+"Mod"
		user_path = FreeCAD.getUserAppDataDir()+"Mod"
		icon_path = ""

		#Check a couple of other places for an icon.		 
		if os.path.exists(user_path + image_path):
			icon_path = user_path + image_path
		elif os.path.exists(global_path + image_path):
			icon_path = global_path + image_path
		return {"MenuText": "Iconify",
			"ToolTip": "Creates a SVG Icon for the Hightlighted Sketch",
			'Pixmap' : str(icon_path) } 

	def IsActive(self):
		if FreeCAD.ActiveDocument == None:
			return False
		else:
			return True

	def Activated(self):

		selection = FreeCADGui.Selection.getCompleteSelection()
		print( "Activated ",selection[0].TypeId )

		if (selection[0].TypeId == 'Sketcher::SketchObject' or selection[0].TypeId == 'Sketcher::SketchObjectPython'):
			print ( "selected ", selection[0].TypeId )
			newcode = Iconify( selection[0] )
			newcode.iconifySketch()

class Iconify:

	sketchObject = "";

	user_path = FreeCAD.getUserAppDataDir()+"Mod/"
	mod = "sketchershapes"
	sketchname = "";

	cmd = "commands/"
	print ( user_path )

	def __init__(self, obj):
	

		self.sketchObject = obj;

	def iconifySketch( self ):
				


		#Get the sketch to draw the SVG
		selection = FreeCADGui.Selection.getSelectionEx()
		## CHANGE Sketch to Match it's name ( not the Label )
		#sketch = FreeCAD.ActiveDocument.Linkage

		self.sketchname = self.sketchObject.Label

		sketch = self.sketchObject

		geometry =sketch.Geometry

		bbox = sketch.Shape.BoundBox

		w = bbox.XMax - bbox.XMin
		h = bbox.YMax - bbox.YMin

		#Create the SVG
		svg = pysvg.structure.Svg( "Test1","Test2",height=h,width=w, viewBox = "0 0 {} {}".format( w*1.2,h*1.2) )

		sb = pysvg.builders.ShapeBuilder()
		gr = pysvg.gradient.LinearGradient( )
		tr = pysvg.builders.TransformBuilder()
		st = pysvg.style.Style()

		def addLine( myline, stroke_color ):
			startX = myline.StartPoint.x
			startY = myline.StartPoint.y
			endX = myline.EndPoint.x
			endY =myline.EndPoint.y
			line = sb.createLine(   startX,startY,endX,endY, strokewidth=2, stroke=stroke_color)
			return line

		def addCircle( mycircle ):
			centerX = mycircle.Center.x
			centerY = mycircle.Center.y
			radius = mycircle.Radius
			circle = sb.createCircle( centerX,centerY,radius, strokewidth=2, stroke="black" )
			return circle

		def addArc(myarc):
			centerX = myarc.Center.x
			centerY = myarc.Center.y
			startx = myarc.StartPoint.x
			starty = myarc.StartPoint.y
			xradius = myarc.Radius
			yradius = myarc.Radius
			endX = myarc.EndPoint.x
			endY =myarc.EndPoint.y

			path = pysvg.shape.Path()
			path.set_style( "opacity:1;fill:none;stroke:#000000;stroke-width:2;stroke-miterlimit:8;")
			path.appendMoveToPath(starty,startx)
			path.appendArcToPath( xradius,yradius,endY,endX,0,0,0,False )
			return path

		def addRedDot( myline ):
			circle = []

			startX = myline.StartPoint.x
			startY = myline.StartPoint.y
			endX = myline.EndPoint.x
			endY =myline.EndPoint.y

			circle.append( sb.createCircle( startX,startY,4, strokewidth=2, stroke="black", fill="red" )  )
			circle.append( sb.createCircle( startX,startY,2, strokewidth=1, stroke="red", fill="red")	)

			circle.append( sb.createCircle( endX,endY,4, strokewidth=2, stroke="black", fill="red")	 )
			circle.append( sb.createCircle( endX,endY,2, strokewidth=1, stroke="red", fill="red")	)

			return circle


		for object in geometry:

#			if not object.Construction:
			if type( object ) == Part.LineSegment:		
				element = addLine( object, "black" )
				decorators = addRedDot(object)
				svg.addElement( element )
				for decorator in decorators:
					svg.addElement(  decorator )

			if type( object ) == Part.Circle:	
				element = addCircle ( object )
				svg.addElement( element )	

			if type( object ) == Part.ArcOfCircle:
				element = addArc( object )
				svg.addElement( element )

#			if object.Construction:
#				if type( object ) == Part.LineSegment:
#					element = addLine( object, "blue" )
#					decorator = addRedDot(object)
#					svg.addElement( element )
#				if type( object ) == Part.Circle:		
#					pass
			
		#svg.save( '/home/randall/Dropbox/Current/MYCODE/FreeCAD/MyWorkbenches/sketchershapes/icons/new_icon.svg' )
		file_name  = self.user_path + self.mod + "/icons/" + self.sketchname + ".svg"
		print ( file_name )
		svg.save ( file_name )

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))
	
	def execute(self,fp):	
		FreeCADGui.SendMsgToActiveView("ViewFit")	
		fp.recompute()


FreeCADGui.addCommand('Iconify', Iconify_Command())

