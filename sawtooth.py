import FreeCAD,FreeCADGui,Part, Sketcher
import os

class SawTooth_Command:

	def GetResources(self):

		image_path = '/sketchershapes/icons/sawtooth.png'
		global_path = FreeCAD.getHomePath()+"Mod"
		user_path = FreeCAD.getUserAppDataDir()+"Mod"
		icon_path = ""
		 
		if os.path.exists(user_path + image_path):
			icon_path = user_path + image_path
		elif os.path.exists(global_path + image_path):
			icon_path = global_path + image_path
		return {"MenuText": "SawTooth Menu",
			"ToolTip": "Create a sketch with a sawtooth shape.",
			'Pixmap' : str(icon_path) } 

	def IsActive(self):
		if FreeCAD.ActiveDocument == None:
			return False
		else:
			return True

	def Activated(self):

		b=FreeCAD.ActiveDocument.addObject("Sketcher::SketchObjectPython","SawTooth")
		newsawtoothobject = SawTooth(b)
		b.ViewObject.Proxy=0
		FreeCAD.ActiveDocument.recompute() 

class SawTooth:


	def __init__(self, obj):

		obj.Proxy = self

		App = FreeCAD
		geometryList = []
		constructionList = []
		constraintList = []

		geometryList.append( Part.LineSegment( App.Vector (-90.23164348809809, -59.25280679149065, 0.0), App.Vector (15.103109050558546, 80.50416052228915, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (15.103109050558546, 80.50416052228915, 0.0), App.Vector (38.7289022060409, 18.93062263256478, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (38.7289022060409, 18.93062263256478, 0.0), App.Vector (-1.9524149099841166, 27.656420777088755, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-1.9524149099841166, 27.656420777088755, 0.0), App.Vector (17.641843310969232, -9.04745041280306, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (17.641843310969232, -9.04745041280306, 0.0), App.Vector (-23.039473805055792, -0.32165226827908827, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-23.039473805055792, -0.32165226827908827, 0.0), App.Vector (-3.445215584102433, -37.0255234581709, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-3.445215584102433, -37.0255234581709, 0.0), App.Vector (-44.12653270012745, -28.29972531364693, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-44.12653270012745, -28.29972531364693, 0.0), App.Vector (-24.532274479174106, -65.00359650353874, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-24.532274479174106, -65.00359650353874, 0.0), App.Vector (-90.23164348809809, -59.25280679149065, 0.0)))

		constructionList.append( Part.LineSegment( App.Vector (-44.12653270012745, -28.29972531364693, 0.0), App.Vector (-1.952414909984115, 27.656420777088755, 0.0)))
		constructionList.append( Part.LineSegment( App.Vector (-24.532274479174106, -65.00359650353874, 0.0), App.Vector (38.7289022060409, 18.930622632564777, 0.0)))

		constraintList.append( Sketcher.Constraint('Coincident',1,1,0,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',4,1,3,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',6,1,5,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',7,1,6,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',0,1,8,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',5,1,4,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',3,1,2,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',2,1,1,2 ) )
		constraintList.append( Sketcher.Constraint('Equal',7,6))
		constraintList.append( Sketcher.Constraint('Equal',6,5))
		constraintList.append( Sketcher.Constraint('Equal',5,4))
		constraintList.append( Sketcher.Constraint('Equal',4,3))
		constraintList.append( Sketcher.Constraint('Equal',3,2))
		constraintList.append( Sketcher.Constraint('Equal',1,8))
		constraintList.append( Sketcher.Constraint('Coincident',9,1,6,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',9,2,2,2 ) )
		constraintList.append( Sketcher.Constraint('PointOnObject',4,2,9 ) )
		constraintList.append( Sketcher.Constraint('Coincident',10,1,8,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',10,2,1,2 ) )
		constraintList.append( Sketcher.Constraint('PointOnObject',3,2,10 ) )
		constraintList.append( Sketcher.Constraint('PointOnObject',5,2,10 ) )
		constraintList.append( Sketcher.Constraint('Coincident',7,2,8,1 ) )

		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

		FreeCADGui.SendMsgToActiveView("ViewFit")	


	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))

	def execute(self,fp):	
		fp.recompute()
			

FreeCADGui.addCommand('SawTooth', SawTooth_Command())

