import Sketcher
import FreeCAD
import FreeCAD
import FreeCADGui
import Part
import os


class Linkage_Command:
    def GetResources(slf):
        #print ('Run getResources() for Linkage_Command' )
        image_path = '/sketchershapes/icons/linkage.png'
        global_path = FreeCAD.getHomePath()+'Mod'
        user_path = FreeCAD.getUserAppDataDir()+'Mod'
        icon_path = ''

        if os.path.exists(user_path + image_path):
            icon_path = user_path + image_path
        elif os.path.exists(global_path + image_path):
            icon_path = global_path + image_path
        return {'MenuText': 'Linkage Menu',
                'ToolTip': 'Tooltip for Linkage command',
                'Pixmap': str(icon_path)}

    def IsActive(self):
        if FreeCAD.ActiveDocument == None:
            # print 'Linkage command is NOT active'
            return False
        else:
            # print 'Linkage command IS active'
            return True

    def Activated(self):

        # print 'LinkageCommand activated'

        b = FreeCAD.ActiveDocument.addObject(
            'Sketcher::SketchObjectPython', 'Linkage')
        newsampleobject = Linkage(b)
        b.ViewObject.Proxy = 0
        FreeCAD.ActiveDocument.recompute()


class Linkage:

    def __init__(self, obj):

        obj.Proxy = self
        tmpCircle = None
        App = FreeCAD
        geometryList = []
        constructionList = []
        constraintList = []

        tmpCircle = Part.Circle(App.Vector(-36.524511735261925, -
                                           21.085849009838892, 0.0), App.Vector(0.0, 0.0, 1.0), 6.0)
        geometryList.append(Part.ArcOfCircle(
            tmpCircle, 0.963164813128, 6.3867992577))
        tmpCircle = Part.Circle(App.Vector(
            49.58435550050076, 29.75963243516066, 0.0), App.Vector(0.0, 0.0, 1.0), 6.0)
        geometryList.append(Part.ArcOfCircle(
            tmpCircle, 4.10475746672, 9.52839191129))
        geometryList.append(Part.LineSegment(App.Vector(-33.098964555425304, -
                                                        16.159833731877065, 0.0), App.Vector(43.61653424841416, 29.139060518986973, 0.0)))
        geometryList.append(Part.LineSegment(App.Vector(-30.556690483175323, -
                                                        20.4652770936652, 0.0), App.Vector(46.15880832066414, 24.833617157198837, 0.0)))
        geometryList.append(Part.Circle(
            App.Vector(-36.524511735261925, -21.085849009838892, 0.0), App.Vector(0.0, 0.0, 1.0), 3.0))
        geometryList.append(Part.Circle(App.Vector(
            49.58435550050076, 29.75963243516066, 0.0), App.Vector(0.0, 0.0, 1.0), 3.0))
        constructionList.append(Part.LineSegment(App.Vector(
            43.61653424841416, 29.139060518986973, 0.0), App.Vector(46.158808320664136, 24.833617157198834, 0.0)))
        constructionList.append(Part.LineSegment(App.Vector(-33.098964555425304, -
                                                            16.159833731877065, 0.0), App.Vector(-30.556690483175323, -20.4652770936652, 0.0)))

        constraintList.append(Sketcher.Constraint('Coincident', 2, 1, 0, 1))
        constraintList.append(Sketcher.Constraint('Coincident', 2, 2, 1, 2))
        constraintList.append(Sketcher.Constraint('Coincident', 3, 1, 0, 2))
        constraintList.append(Sketcher.Constraint('Coincident', 3, 2, 1, 1))
        constraintList.append(Sketcher.Constraint('Coincident', 5, 3, 1, 3))
        constraintList.append(Sketcher.Constraint('Coincident', 4, 3, 0, 3))
        constraintList.append(Sketcher.Constraint('Coincident', 6, 2, 1, 1))
        constraintList.append(Sketcher.Constraint('Coincident', 6, 1, 1, 2))
        constraintList.append(Sketcher.Constraint('Coincident', 7, 1, 0, 1))
        constraintList.append(Sketcher.Constraint('Coincident', 7, 2, 0, 2))
        constraintList.append(Sketcher.Constraint('Distance', 0, 2, 0, 1, 5.0))
        constraintList.append(Sketcher.Constraint('Equal', 0, 1))
        constraintList.append(Sketcher.Constraint('Equal', 7, 6))
        constraintList.append(Sketcher.Constraint('Radius', 0, 6.0))
        constraintList.append(Sketcher.Constraint('Radius', 4, 3.0))
        constraintList.append(Sketcher.Constraint('Equal', 4, 5))
        constraintList.append(Sketcher.Constraint('Perpendicular', 2, 0, 6))
        constraintList.append(Sketcher.Constraint('Perpendicular', 2, 0, 7))
        named_constriant = Sketcher.Constraint('Distance', 0, 3, 1, 3, 100.0)
        named_constriant.Name = "Distance to Centers"
        constraintList.append(named_constriant)

        obj.addGeometry(geometryList, False)
        obj.addGeometry(constructionList, True)
        obj.addConstraint(constraintList)

        FreeCADGui.SendMsgToActiveView("ViewFit")

    def onChanged(self, fp, prop):
        name = str(prop)
        newvalue = str(fp.getPropertyByName(str(prop)))
        FreeCAD.Console.PrintMessage(
            'Changed property: ' + name + 'to ' + newvalue + '')

    def execute(self, fp):
        fp.recompute()
        #print (' Linkage Class executed()')


FreeCADGui.addCommand('Linkage', Linkage_Command())
