import FreeCAD
import FreeCADGui
import os

from FreeCADGui import Workbench


class SketcherShapes (Workbench):
	"Selection of Sketch based Shapes"
	Icon = """
			/* XPM */
			static const char * main_xpm[]={
			"18 18 52 1",
			" 	c None",
			".	c #FF0000",
			"+	c #F40808",
			"@	c #FF0101",
			"#	c #E31414",
			"$	c #FF5555",
			"%	c #FF2020",
			"&	c #D41D1D",
			"*	c #FF8A8A",
			"=	c #FF2121",
			"-	c #C72626",
			";	c #FF8585",
			">	c #FF1818",
			",	c #FD0202",
			"'	c #715D5D",
			")	c #BB2D2D",
			"!	c #FF1515",
			"~	c #7C5757",
			"{	c #705F5F",
			"]	c #7B5A5A",
			"^	c #FB0404",
			"/	c #FD0101",
			"(	c #725D5D",
			"_	c #9E4141",
			":	c #A83A3A",
			"<	c #DE1818",
			"[	c #805454",
			"}	c #904A4A",
			"|	c #666666",
			"1	c #735D5D",
			"2	c #7B5858",
			"3	c #E80F0F",
			"4	c #914A4A",
			"5	c #7A5A5A",
			"6	c #F90303",
			"7	c #FE0101",
			"8	c #E61111",
			"9	c #954646",
			"0	c #E90E0E",
			"a	c #676767",
			"b	c #934848",
			"c	c #C22A2A",
			"d	c #FF0707",
			"e	c #EC0C0C",
			"f	c #8E4C4C",
			"g	c #FF3333",
			"h	c #FFF4F4",
			"i	c #C32828",
			"j	c #F70606",
			"k	c #F50707",
			"l	c #F30808",
			"m	c #A43C3C",
			"             ...  ",
			"    .+     ...... ",
			"   @..#    .    . ",
			"  ..$%.&  ..  . ..",
			" ...*=..-  .    ..",
			"....;>..,  ..  .. ",
			" ').;!.~{]^../... ",
			"  .....( ../_:<   ",
			"  .....[../}|     ",
			"   1  23./45      ",
			"      ../46....78 ",
			" ......794......0a",
			" ..  ./b|c..d=..e|",
			"..    7f  ..gh..e|",
			".. .  .i  ......e|",
			"..    .   ......0|",
			" ......   jkkkklm|",
			"  ....     |aaaa||"};
			"""

	MenuText = "Sketcher Shapes Workbench"
	ToolTip = "A workbench for educational purposes,ie., how to make a workbench"

	def GetClassName(self):
		return "Gui::PythonWorkbench"

	def Initialize(self):
		import trapezoid, parallelogram
		import keyhole, arrow, sawtooth, plus, cancel
		import camshape, linkage
		import roundedsquare, centeredsquare
		import holes, microusb, pcb_bracket
		import corner,ushape, ibeamprofile, flangepart
		import codify, iconify

		self.appendToolbar("Sketcher Shapes", ["Trapezoid", "Parallelogram", "Keyhole","CamShape", "Linkage", "Arrow", "RoundedSquare","CenteredSquare", "SawTooth","Holes", "Plus","Cancel", "MicroUSB", "BoardBrackets","Corner","UShape","IBeamProfile","FlangePart","Codify","Iconify"])
		self.appendMenu("Sketcher Shapes", ["Trapezoid", "Parallelogram", "Keyhole","CamShape", "Linkage","UShape", "Arrow", "RoundedSquare",
											"CenteredSquare", "SawTooth", "Holes", "Plus","Cancel", "MicroUSB", "BoardBrackets","Corner","IBeamProfile","FlangePart","Codify","Iconify"])


# 		Log ("The SketcherShapes Module Initialize method has been run \n")

# shape_commands = open( "shape_commands.conf", "r" )

# new_commands = __import__(shape_commands.readln(1))
# self.appendToolBar("Sketcher Shapes",[shape_commands.readln(2)])
# self.appendMenu("Sketcher Shapes",[shape_commands.readln(3)])

	def Activated(self):
		# do something here if needed...
		# Msg("SketcherShapes.Activated()\n")
		pass

	def Deactivated(self):
		# do something here if needed...
		# Msg("SketcherShapes.Deactivated()\n")
		pass


FreeCADGui.addWorkbench(SketcherShapes)
