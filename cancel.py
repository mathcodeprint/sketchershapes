import Sketcher
import FreeCAD
import FreeCAD,FreeCADGui,Part, Draft
import os

__title__="FreeCAD Stick Framers Toolkit"
__author__ = "Paul Randall"
__url = "http://www.mathcodeprint.com/"
__command_name__ = "Cancel" #Name of the command to appear in Toolbar
__command_group__= "" #Name of Toolbar to assign the command

class Cancel_Command:
	def GetResources(slf):
		#print 'Run getResources() for Cancel_Command' 
		image_path = '/sketchershapes/icons/cancel.png' 
		global_path = FreeCAD.getHomePath()+'Mod' 
		user_path = FreeCAD.getUserAppDataDir()+'Mod' 
		icon_path = '' 

		if os.path.exists(user_path + image_path): 
			icon_path = user_path + image_path 
		elif os.path.exists(global_path + image_path): 
			icon_path = global_path + image_path 
		return {'MenuText': 'Cancel', 
			'ToolTip': 'Tooltip for Cancel command', 
			'Pixmap' : str(icon_path) }  

	def IsActive(self): 
		if FreeCAD.ActiveDocument == None: 
			#print 'Cancel command is NOT active' 
			return False 
		else: 
			#print 'Cancel command IS active' 
			return True 
 
	def Activated(self): 
 
		#print 'CancelCommand activated' 
 
		b=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','Cancel') 
		newsampleobject = Cancel(b) 
		b.ViewObject.Proxy=0 
		FreeCAD.ActiveDocument.recompute()  
class Cancel: 
 
 
	def __init__(self, obj):
 
		#print 'The Cancel class has been instantiated init' 
		obj.Proxy = self
		App = FreeCAD
		tmpCircle = None
		geometryList = [] 
		constructionList = [] 
		constraintList = [] 
 
		geometryList.append( Part.Circle ( App.Vector (0.0, 0.0, 0.0), App.Vector (0.0, 0.0, 1.0), 13.0))
		tmpCircle = Part.Circle ( App.Vector (0.0, 0.0, 0.0), App.Vector (0.0, 0.0, 1.0), 10.0)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 2.444997626072948, 5.285453734109362))
		tmpCircle = Part.Circle ( App.Vector (0.0, 0.0, 0.0), App.Vector (0.0, 0.0, 1.0), 10.0)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 5.586590279662741, 8.427046387699155))
		geometryList.append( Part.LineSegment( App.Vector (-7.670312928203239, 6.416096911942514, 0.0), App.Vector (5.422097295410526, -8.402431845549351, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-5.422097295410527, 8.402431845549351, 0.0), App.Vector (7.670312928203238, -6.416096911942514, 0.0)))


		constraintList.append( Sketcher.Constraint('Coincident',0,3,-1,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',1,3,0,3 ) )
		constraintList.append( Sketcher.Constraint('Equal',3,4))
		constraintList.append( Sketcher.Constraint('Coincident',1,2,3,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',4,2,2,1 ) )
		constraintList.append( Sketcher.Constraint('Radius',1,10.0 ))
		constraintList.append( Sketcher.Constraint('Radius',0,13.0 ))
		constraintList.append( Sketcher.Constraint('Equal',1,2))
		constraintList.append( Sketcher.Constraint('Coincident',2,3,0,3 ) )
		constraintList.append( Sketcher.Constraint('Coincident',4,1,2,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',3,1,1,1 ) )
		constraintList.append( Sketcher.Constraint('Parallel',3,4))
		constraintList.append( Sketcher.Constraint('Distance',1,1,2,2,3.0))


		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))
		#FreeCAD.Console.print.writeMessage('Changed property: ' + name + 'to ' + newvalue + '') 

	def execute(self,fp):	
		fp.recompute()
		print (' Cancel Class executed() ') 
FreeCADGui.addCommand('Cancel',Cancel_Command() ) 
