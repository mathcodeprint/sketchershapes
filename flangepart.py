import Sketcher
import FreeCAD
import FreeCAD,FreeCADGui,Part, Draft
import os

__title__="FreeCAD Stick Framers Toolkit"
__author__ = "Paul Randall"
__url = "http://www.mathcodeprint.com/"
__command_name__ = "FlangePart" #Name of the command to appear in Toolbar
__command_group__= "" #Name of Toolbar to assign the command

class FlangePart_Command:
	def GetResources(slf):
		#print 'Run getResources() for FlangePart_Command' 
		image_path = '/sketchershapes/icons/flangepart.png' 
		global_path = FreeCAD.getHomePath()+'Mod' 
		user_path = FreeCAD.getUserAppDataDir()+'Mod' 
		icon_path = '' 

		if os.path.exists(user_path + image_path): 
			icon_path = user_path + image_path 
		elif os.path.exists(global_path + image_path): 
			icon_path = global_path + image_path 
		return {'MenuText': 'FlangePart', 
			'ToolTip': 'Tooltip for FlangePart command', 
			'Pixmap' : str(icon_path) }  

	def IsActive(self): 
		if FreeCAD.ActiveDocument == None: 
			#print 'FlangePart command is NOT active' 
			return False 
		else: 
			#print 'FlangePart command IS active' 
			return True 
 
	def Activated(self): 
 
		#print 'FlangePartCommand activated' 
 
		b=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','FlangePart') 
		newsampleobject = FlangePart(b) 
		b.ViewObject.Proxy=0 
		FreeCAD.ActiveDocument.recompute()  
class FlangePart: 
 
 
	def __init__(self, obj):
 
		#print 'The FlangePart class has been instantiated init' 
		obj.Proxy = self
		App = FreeCAD
		tmpCircle = None
		geometryList = [] 
		constructionList = [] 
		constraintList = [] 
 
		tmpCircle = Part.Circle ( App.Vector (-6.572785759664116e-32, -2.1710338192281604e-31, 0.0), App.Vector (0.0, 0.0, 1.0), 22.45072523222012)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 0.3094833191868908, 1.3566808703834885))
		geometryList.append( Part.LineSegment( App.Vector (4.770401193050804, 21.93805679430119, 0.0), App.Vector (6.682749693750514, 30.732539338025504, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (21.38411509005604, 6.837739777725005, 0.0), App.Vector (29.956534636409934, 9.578838667092132, 0.0)))
		tmpCircle = Part.Circle ( App.Vector (-6.572785759664116e-32, -2.1710338192281604e-31, 0.0), App.Vector (0.0, 0.0, 1.0), 31.450725232220123)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 0.3094833191868908, 1.0702742858205072))
		tmpCircle = Part.Circle ( App.Vector (11.000318736161729, 29.46423434157855, 0.0), App.Vector (0.0, 0.0, 1.0), 4.5)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 5.8542649123459425, 9.139060858217226))
		geometryList.append( Part.Circle ( App.Vector (11.000318736161729, 29.46423434157855, 0.0), App.Vector (0.0, 0.0, 1.0), 2.5))


		constraintList.append( Sketcher.Constraint('Coincident',0,3,-1,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',1,1,0,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',2,1,0,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',3,3,0,3 ) )
		constraintList.append( Sketcher.Constraint('Coincident',3,1,2,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',4,2,1,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',4,1,3,2 ) )
		constraintList.append( Sketcher.Constraint('PointOnObject',4,3,3 ) )
		constraintList.append( Sketcher.Constraint('PointOnObject',1,2,3 ) )
		constraintList.append( Sketcher.Constraint('Perpendicular',0,0,1 ) )
		constraintList.append( Sketcher.Constraint('Perpendicular',0,0,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',5,3,4,3 ) )
		constraintList.append( Sketcher.Constraint('Radius',5,2.5 ))
		constraintList.append( Sketcher.Constraint('Radius',4,4.5 ))
		constraintList.append( Sketcher.Constraint('Distance',2,0,-2000,0,9.0))


		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))
		#FreeCAD.Console.print.writeMessage('Changed property: ' + name + 'to ' + newvalue + '') 

	def execute(self,fp):	
		fp.recompute()
		print (' FlangePart Class executed() ') 
FreeCADGui.addCommand('FlangePart',FlangePart_Command() ) 
