import FreeCAD,FreeCADGui,Part, Sketcher
import os

class RoundedSquare_Command:

	def GetResources(self):
		image_path = '/sketchershapes/icons/roundedsquare.svg'
		global_path = FreeCAD.getHomePath()+"Mod"
		user_path = FreeCAD.getUserAppDataDir()+"Mod"
		icon_path = ""
		 
		if os.path.exists(user_path + image_path):
			icon_path = user_path + image_path
		elif os.path.exists(global_path + image_path):
			icon_path = global_path + image_path
		return {"MenuText": "Rounded Square",
			"ToolTip": "Create a sketch of a square with rounded corners",
			'Pixmap' : str(icon_path) } 

	def IsActive(self):
		if FreeCAD.ActiveDocument == None:
			return False
		else:
			return True

	def Activated(self):

		c=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','RoundedSquare')
		sketcherObject = RoundedSquare(c)
		c.ViewObject.Proxy=0
		FreeCAD.ActiveDocument.recompute()

class RoundedSquare:


	def __init__(self, obj):


		obj.Proxy = self

		App = FreeCAD
		geometryList = []
		constructionList = []
		constraintList = []

		geometryList.append( Part.LineSegment( App.Vector (-20.18770044760085, -45.73550467900528, 0.0), App.Vector (18.384821137371997, -45.73550467900528, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-33.837700447600845, -32.08550467900528, 0.0), App.Vector (-33.837700447600845, 32.02125654878242, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-20.187700447600847, 45.671256548782424, 0.0), App.Vector (18.384821137372, 45.671256548782424, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (32.034821137372, 32.021256548782425, 0.0), App.Vector (32.034821137372, -32.085504679005275, 0.0)))
		tmpCircle = Part.Circle ( App.Vector (-20.187700447600847, -32.08550467900528, 0.0), App.Vector (0.0, 0.0, 1.0), 13.65)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 3.14159265359, 4.71238898038))
		tmpCircle = Part.Circle ( App.Vector (-20.187700447600843, 32.021256548782425, 0.0), App.Vector (0.0, 0.0, 1.0), 13.65)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 1.57079632679, 3.14159265359))
		tmpCircle = Part.Circle ( App.Vector (18.384821137372, 32.021256548782425, 0.0), App.Vector (0.0, 0.0, 1.0), 13.65)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 6.94614168202e-17, 1.57079632679))
		tmpCircle = Part.Circle ( App.Vector (18.384821137372, -32.08550467900528, 0.0), App.Vector (0.0, 0.0, 1.0), 13.65)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 4.71238898038, 6.28318530718))


		constraintList.append( Sketcher.Constraint('Tangent',4,1,1,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',1,2,5,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',5,1,2,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',2,2,6,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',6,1,3,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',3,2,7,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',7,1,0,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',0,1,4,2 ) )
		constraintList.append( Sketcher.Constraint('Equal',4,5))
		constraintList.append( Sketcher.Constraint('Equal',5,6))
		constraintList.append( Sketcher.Constraint('Equal',6,7))
		constraintList.append( Sketcher.Constraint('Radius',4,13.65 ))
		constraintList.append( Sketcher.Constraint('Equal',0,2))
		constraintList.append( Sketcher.Constraint('Equal',1,3))
		constraintList.append( Sketcher.Constraint('Horizontal',0 ) )
		constraintList.append( Sketcher.Constraint('Vertical',1 ) )

		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

		FreeCADGui.SendMsgToActiveView("ViewFit")	


	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))

	def execute(self,fp):
		
		fp.recompute()

FreeCADGui.addCommand('RoundedSquare', RoundedSquare_Command())

