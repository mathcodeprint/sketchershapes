import FreeCAD,FreeCADGui,Part, Sketcher
import os

class CenteredSquare_Command:

	def GetResources(self):
		#Check the modules directory for the icon.
		image_path = '/sketchershapes/icons/centeredsquare.svg'
		global_path = FreeCAD.getHomePath()+"Mod"
		user_path = FreeCAD.getUserAppDataDir()+"Mod"
		icon_path = ""

 		#Check a couple of other places for an icon.		 
		if os.path.exists(user_path + image_path):
			icon_path = user_path + image_path
		elif os.path.exists(global_path + image_path):
			icon_path = global_path + image_path
		return {"MenuText": "Centered Square",
			"ToolTip": "Adds a 20mm x 20mm square centered on the sketch origin",
			'Pixmap' : str(icon_path) } 

	def IsActive(self):
		if FreeCAD.ActiveDocument == None:
			return False
		else:
			return True

	def Activated(self):
		c=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','CenteredSquare')
		sketcherObject = CenteredSquare(c)
		c.ViewObject.Proxy=0
		FreeCAD.ActiveDocument.recompute()

class CenteredSquare:

	def __init__(self, obj):

		obj.Proxy = self

		App = FreeCAD
		geometryList = []
		constructionList = []
		constraintList = []
		
		#Add Geometries

		geometryList.append(Part.LineSegment(App.Vector(-10,10,0),App.Vector(10,10,0)))
		geometryList.append(Part.LineSegment(App.Vector(10,10,0),App.Vector(10,-10,0)))
		geometryList.append(Part.LineSegment(App.Vector(10,-10,0),App.Vector(-10,-10,0)))
		geometryList.append(Part.LineSegment(App.Vector(-10,-10,0),App.Vector(-10,10,0)))

		(l1,l2,l3,l4) = obj.addGeometry(geometryList)

		#Add Construction Lines

		#cLines = obj.addGeometry( constructionList, True)

		#Add Constraints	
	
		constraintList.append(Sketcher.Constraint('Coincident',l1,2,l2,1))
		constraintList.append(Sketcher.Constraint('Coincident',l2,2,l3,1))
		constraintList.append(Sketcher.Constraint('Coincident',l3,2,l4,1))
		constraintList.append(Sketcher.Constraint('Coincident',l4,2,l1,1))

		constraintList.append(Sketcher.Constraint('Perpendicular',l1,l2))

		constraintList.append(Sketcher.Constraint('Equal',l1,l3))
		constraintList.append(Sketcher.Constraint('Equal',l2,l4))

		#constraintList.append(Sketcher.Constraint('Symmetric',l1,1,l1,2,-2)) 
		#constraintList.append(Sketcher.Constraint('Symmetric',l1,2,l2,2,-1)) 
		constraintList.append(Sketcher.Constraint('Distance',l1,1,l1,2,20)) 
		constraintList.append(Sketcher.Constraint('Distance',l2,2,l2,1,20)) 

		obj.addConstraint( constraintList )

		FreeCADGui.SendMsgToActiveView("ViewFit")	


	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))

	def execute(self,fp):
		fp.recompute()

FreeCADGui.addCommand('CenteredSquare', CenteredSquare_Command())
