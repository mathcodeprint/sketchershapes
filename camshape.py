import Sketcher
import FreeCAD
import FreeCAD,FreeCADGui,Part, Draft
import os

__title__="FreeCAD Stick Framers Toolkit"
__author__ = "Paul Randall"
__url = "http://www.mathcodeprint.com/"
__command_name__ = "CamShape" #Name of the command to appear in Toolbar
__command_group__= "" #Name of Toolbar to assign the command

class CamShape_Command:
	def GetResources(slf):
		#print 'Run getResources() for CamShape_Command' 
		image_path = '/sketchershapes/icons/camshape.png' 
		global_path = FreeCAD.getHomePath()+'Mod' 
		user_path = FreeCAD.getUserAppDataDir()+'Mod' 
		icon_path = '' 

		if os.path.exists(user_path + image_path): 
			icon_path = user_path + image_path 
		elif os.path.exists(global_path + image_path): 
			icon_path = global_path + image_path 
		return {'MenuText': 'CamShape', 
			'ToolTip': 'Tooltip for CamShape command', 
			'Pixmap' : str(icon_path) }  

	def IsActive(self): 
		if FreeCAD.ActiveDocument == None: 
			#print 'CamShape command is NOT active' 
			return False 
		else: 
			#print 'CamShape command IS active' 
			return True 
 
	def Activated(self): 
 
		#print 'CamShapeCommand activated' 
 
		b=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','CamShape') 
		newsampleobject = CamShape(b) 
		b.ViewObject.Proxy=0 
		FreeCAD.ActiveDocument.recompute()  
class CamShape: 
 
 
	def __init__(self, obj):
 
		#print 'The CamShape class has been instantiated init' 
		obj.Proxy = self
		App = FreeCAD
		tmpCircle = None
		geometryList = [] 
		constructionList = [] 
		constraintList = [] 
 
		geometryList.append( Part.LineSegment( App.Vector (-32.45378065315402, 57.78372746113255, 0.0), App.Vector (64.36906070361783, 72.79496963496314, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (15.247749816232195, -70.82639652169479, 0.0), App.Vector (98.44158246746514, -19.069404638484954, 0.0)))
		tmpCircle = Part.Circle ( App.Vector (72.02945314762457, 23.385270307768234, 0.0), App.Vector (0.0, 0.0, 1.0), 50.0)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 5.26891845722419, 8.007795269233322))
		tmpCircle = Part.Circle ( App.Vector (-21.72923123154459, -11.389851596940323, 0.0), App.Vector (0.0, 0.0, 1.0), 70.0)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 1.7246099620537354, 5.26891845722419))


		constraintList.append( Sketcher.Constraint('Tangent',3,2,1,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',2,2,0,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',3,1,0,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',1,2,2,1 ) )
		constraintList.append( Sketcher.Constraint('Radius',2,50.0 ))
		named_constraint = Sketcher.Constraint('Distance',3,3,2,3,100.0)
		named_constraint.Name = "CenterOffset"
		constraintList.append( named_constraint )
		constraintList.append( Sketcher.Constraint('Radius',3,70.0 ))


		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))
		#FreeCAD.Console.print.writeMessage('Changed property: ' + name + 'to ' + newvalue + '') 

	def execute(self,fp):	
		fp.recompute()
		print (' CamShape Class executed() ') 
FreeCADGui.addCommand('CamShape',CamShape_Command() ) 
