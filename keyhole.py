import Sketcher
import FreeCAD
import FreeCAD,FreeCADGui,Part
import os
class Keyhole_Command:
	def GetResources(slf):
		image_path = '/sketchershapes/icons/Keyhole.png' 
		global_path = FreeCAD.getHomePath()+'Mod' 
		user_path = FreeCAD.getUserAppDataDir()+'Mod' 
		icon_path = '' 

		if os.path.exists(user_path + image_path): 
			icon_path = user_path + image_path 
		elif os.path.exists(global_path + image_path): 
			icon_path = global_path + image_path 
		return {'MenuText': 'Keyhole', 
			'ToolTip': 'Create a sketch of a keyhole opening', 
			'Pixmap' : str(icon_path) }  

	def IsActive(self): 
		if FreeCAD.ActiveDocument == None: 
			return False 
		else: 
			return True 
 
	def Activated(self): 
		b=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','Keyhole') 
		newsampleobject = Keyhole(b) 
		b.ViewObject.Proxy=0 
		FreeCAD.ActiveDocument.recompute()  
class Keyhole: 
 
 
	def __init__(self, obj): 
 
		obj.Proxy = self
		App = FreeCAD	
		geometryList = []
		constructionList = []
		constraintList = [] 
 
		tmpCircle = Part.Circle ( App.Vector (33.24531930498395, 37.45437196030327, 0.0), App.Vector (0.0, 0.0, 1.0), 22.225)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 2.45665136563, 5.59824401922))
		geometryList.append( Part.LineSegment( App.Vector (16.033030453776167, 51.514519096894276, 0.0), App.Vector (39.18073398407056, 79.85170188274599, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (50.45760815619173, 23.39422482371226, 0.0), App.Vector (73.60531168648612, 51.731407609564, 0.0)))
		tmpCircle = Part.Circle ( App.Vector (80.74591203774804, 95.60411355099822, 0.0), App.Vector (0.0, 0.0, 1.0), 44.45)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 4.55104646802, 9.78703422401))


		constraintList.append( Sketcher.Constraint('Tangent',0,1,1,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',2,1,0,2 ) )
		constraintList.append( Sketcher.Constraint('Equal',2,1))
		constraintList.append( Sketcher.Constraint('Coincident',1,2,3,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',3,1,2,2 ) )
		constraintList.append( Sketcher.Constraint('Radius',3,44.45 ))
		constraintList.append( Sketcher.Constraint('Radius',0,22.225 ))
		constraintList.append( Sketcher.Constraint('Parallel',1,2))


		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

		FreeCADGui.SendMsgToActiveView("ViewFit")	


	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))

	def execute(self,fp):	
		fp.recompute()

FreeCADGui.addCommand('Keyhole',Keyhole_Command() ) 

