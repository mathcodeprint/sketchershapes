import FreeCAD,FreeCADGui,Part
import os

class sswbReload_Command:

	def GetResources(self):
		image_path = '/sketchershapes/icons/reload.png'
		global_path = FreeCAD.getHomePath()+"Mod"
		user_path = FreeCAD.getUserAppDataDir()+"Mod"
		icon_path = ""
		 
		if os.path.exists(user_path + image_path):
			icon_path = user_path + image_path
		elif os.path.exists(global_path + image_path):
			icon_path = global_path + image_path
		return {"MenuText": "Reload Workbench",
			"ToolTip": "Reload my classes",
			'Pixmap' : str(icon_path) } 

	def IsActive(self):
		if FreeCAD.ActiveDocument == None:
			return False
		else:
			return True

	def Activated(self):
		import trapezoid,parallelogram,linkage, keyhole,arrow, roundedsquare, centeredsquare, sawtooth, holes,microusb,pcb_bracket,sswbreload
		reload(trapezoid)
		reload(parallelogram)
		reload(keyhole)
		reload(linkage)
		reload(arrow)
		reload(roundedsquare)
		reload(centeredsquare)
		reload(sawtooth)
		reload(holes)
		reload(microusb)
		reload(pcb_bracket)		
		reload(sswbreload)						

FreeCADGui.addCommand('sswbReload', sswbReload_Command())




