import FreeCAD,FreeCADGui,Part, Sketcher
import os

class Linkage_Command:

	def GetResources(self):
		image_path = '/sketchershapes/icons/linkage.svg'
		global_path = FreeCAD.getHomePath()+"Mod"
		user_path = FreeCAD.getUserAppDataDir()+"Mod"
		icon_path = ""
		 
		if os.path.exists(user_path + image_path):
			icon_path = user_path + image_path
		elif os.path.exists(global_path + image_path):
			icon_path = global_path + image_path
		return {"MenuText": "Linkage",
			"ToolTip": "Create a sketch of a linkage bar.",
			'Pixmap' : str(icon_path) } 

	def IsActive(self):
		if FreeCAD.ActiveDocument == None:
			return False
		else:
			return True

	def Activated(self):

		c=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','Linkage')
		sketcherObject = Linkage(c)
		c.ViewObject.Proxy=0
		FreeCAD.ActiveDocument.recompute()

class Linkage:

	def __init__(self, obj):

	        obj.Proxy = self

		App = FreeCAD
		geometryList = []
		constructionList = []
		constraintList = []

		geometryList.append( Part.ArcOfCircle( Part.Circle( App.Vector(-50,-0.0,0), App.Vector(0,0,1),5.5), 0.429775,5.853410 ))
		geometryList.append( Part.ArcOfCircle( Part.Circle( App.Vector(50,-0.0,0), App.Vector(0,0,1),5.5 ), -2.711817, 2.711817 ))
		geometryList.append( Part.LineSegment(App.Vector(-44.546316,-2.496507,0),App.Vector(44.830833,-2.496507,0)))
		geometryList.append( Part.LineSegment(App.Vector(-44.546316,2.503493,0),App.Vector(44.544970,2.503493,0)))
		geometryList.append( Part.Circle( App.Vector(-50.381489,-0.039078,0), App.Vector(0,0,1),3) )
		geometryList.append( Part.Circle( App.Vector(50.741444,-0.296058,0), App.Vector(0,0,1),3 ) )

		Lines = obj.addGeometry( geometryList, False)

		#Construction Lines
		constructionList.append( Part.LineSegment(App.Vector(44.50,2.501279,0),App.Vector(44.544587,-2.498593,0)))
		constructionList.append( Part.LineSegment(App.Vector(-44.50,2.501279,0),App.Vector(-44.544587,-2.498593,0)))

		cLines = obj.addGeometry( constructionList, True)

		#constraints		
		constraintList.append( Sketcher.Constraint('Coincident',Lines[2],1,Lines[0],1))
		constraintList.append( Sketcher.Constraint('Coincident',Lines[2],2,Lines[1],2)) 
		constraintList.append( Sketcher.Constraint('Coincident',Lines[3],1,Lines[0],2))
		constraintList.append( Sketcher.Constraint('Coincident',Lines[3],2,Lines[1],1))
		constraintList.append( Sketcher.Constraint('Coincident',Lines[5],3,Lines[1],3))
		constraintList.append( Sketcher.Constraint('Coincident',Lines[4],3,Lines[0],3))		
		constraintList.append( Sketcher.Constraint('Coincident',cLines[0],2,Lines[1],1))
		constraintList.append( Sketcher.Constraint('Coincident',cLines[0],1,1,2))
		constraintList.append( Sketcher.Constraint('Coincident',cLines[1],1,0,1))
		constraintList.append( Sketcher.Constraint('Coincident',cLines[1],2,0,2))	
		constraintList.append( Sketcher.Constraint('Distance',Lines[0],2,Lines[0],1,5))
		constraintList.append( Sketcher.Constraint('Equal',Lines[0],Lines[1]))
		constraintList.append( Sketcher.Constraint('Equal',cLines[1],cLines[0]))
		constraintList.append( Sketcher.Constraint('Radius',0,6))
		constraintList.append( Sketcher.Constraint('Radius',4,3))
		constraintList.append( Sketcher.Constraint('Equal',4,5))

		constraintList.append( Sketcher.Constraint('Perpendicular',2,6))
		constraintList.append( Sketcher.Constraint('Perpendicular',2,7))

		named_constriant = Sketcher.Constraint('Distance',Lines[0],3,Lines[1],3,100 )
		named_constriant.Name = "Distance to Centers"
		constraintList.append( named_constriant )

		obj.addConstraint( constraintList )

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))

	def execute(self,fp):
		
		fp.recompute()

FreeCADGui.addCommand('Linkage', Linkage_Command())

