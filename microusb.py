import Sketcher
import FreeCAD
import FreeCAD,FreeCADGui,Part
import os

class MicroUSB_Command:

	def GetResources(slf):
		image_path = '/sketchershapes/icons/MicroUsb.png' 
		global_path = FreeCAD.getHomePath()+'Mod' 
		user_path = FreeCAD.getUserAppDataDir()+'Mod' 
		icon_path = '' 

		if os.path.exists(user_path + image_path): 
			icon_path = user_path + image_path 
		elif os.path.exists(global_path + image_path): 
			icon_path = global_path + image_path 
		return {'MenuText': 'MicroUSB', 
			'ToolTip': 'Create a sketch for a MicroUSB outline', 
			'Pixmap' : str(icon_path) }  

	def IsActive(self): 
		if FreeCAD.ActiveDocument == None: 
			#print 'MicroUSB command is NOT active' 
			return False 
		else: 
			#print 'MicroUSB command IS active' 
			return True 
 
	def Activated(self): 

 
		b=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','MicroUSB') 
		newsampleobject = MicroUSB(b) 
		b.ViewObject.Proxy=0 
		FreeCAD.ActiveDocument.recompute()  
class MicroUSB: 
 
	def __init__(self, obj): 
 

		obj.Proxy = self 
		App = FreeCAD
		geometryList = [] 
		constructionList = [] 
		constraintList = [] 
		 
		geometryList.append( Part.LineSegment( App.Vector (-3.1674950000000006, 1.723095, 0.0), App.Vector (3.1674950000000006, 1.723095, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-4.220495, 0.6700950000000002, 0.0), App.Vector (-4.220495, -0.07490499999999978, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (4.2204950000000006, 0.6700950000000001, 0.0), App.Vector (4.2204950000000006, -0.07490499999999989, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (2.5604349838904237, -1.723095, 0.0), App.Vector (-2.5604349838904237, -1.723095, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (3.3055083592992376, -1.4150157891449515, 0.0), App.Vector (3.917912608219032, -0.8041262443784832, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-3.3055083592992376, -1.4150157891449509, 0.0), App.Vector (-3.917912608219032, -0.8041262443784826, 0.0)))
		tmpCircle = Part.Circle ( App.Vector (-3.167495, 0.6700950000000002, 0.0), App.Vector (0.0, 0.0, 1.0), 1.053)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 1.57079632679, 3.14159265359))
		tmpCircle = Part.Circle ( App.Vector (3.1674950000000006, 0.6700950000000002, 0.0), App.Vector (0.0, 0.0, 1.0), 1.053)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 0.0, 1.57079632679))
		tmpCircle = Part.Circle ( App.Vector (3.1904950000000003, -0.07490499999999999, 0.0), App.Vector (0.0, 0.0, 1.0), 1.03)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 5.49654892661, 6.28318530718))
		tmpCircle = Part.Circle ( App.Vector (-3.190495, -0.0749049999999998, 0.0), App.Vector (0.0, 0.0, 1.0), 1.03)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 3.14159265359, 3.92822903416))
		tmpCircle = Part.Circle ( App.Vector (2.5604349838904237, -0.6680950000000001, 0.0), App.Vector (0.0, 0.0, 1.0), 1.055)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 4.71238898038, 5.49654892661))
		tmpCircle = Part.Circle ( App.Vector (-2.5604349838904237, -0.668095, 0.0), App.Vector (0.0, 0.0, 1.0), 1.055)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 3.92822903416, 4.71238898038))
		constructionList.append( Part.LineSegment( App.Vector (0.0, 1.723095, 0.0), App.Vector (0.0, 0.0, 0.0)))
		constructionList.append( Part.LineSegment( App.Vector (0.0, 2.9442754173176976e-27, 0.0), App.Vector (0.0, -1.723095, 0.0)))
				
		constraintList.append( Sketcher.Constraint('Vertical',2 ) )
		constraintList.append( Sketcher.Constraint('Vertical',1 ) )
		constraintList.append( Sketcher.Constraint('Equal',1,2))
		constraintList.append( Sketcher.Constraint('Equal',5,4))
		constraintList.append( Sketcher.Constraint('Tangent',6,1,0,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',7,2,0,2 ) )
		constraintList.append( Sketcher.Constraint('Equal',11,10))
		constraintList.append( Sketcher.Constraint('Tangent',2,1,7,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',1,1,6,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',5,2,9,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',5,1,11,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',11,2,3,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',4,1,10,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',8,1,4,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',8,2,2,2 ) )
		constraintList.append( Sketcher.Constraint('Radius',10,1.055 ))
		constraintList.append( Sketcher.Constraint('Radius',8,1.03 ))
		constraintList.append( Sketcher.Constraint('Radius',7,1.053 ))
		constraintList.append( Sketcher.Constraint('DistanceY',2,2,2,1,0.745))
		constraintList.append( Sketcher.Constraint('Tangent',3,1,10,1 ) )
		constraintList.append( Sketcher.Constraint('Equal',6,7))
		constraintList.append( Sketcher.Constraint('Tangent',1,2,9,1 ) )
		constraintList.append( Sketcher.Constraint('Distance',4,0,-2000,0,0.865))
		constraintList.append( Sketcher.Constraint('Symmetric',3,2,3,1,-2))
		constraintList.append( Sketcher.Constraint('Symmetric',0,1,0,2,-2))
		constraintList.append( Sketcher.Constraint('DistanceY',3,2,0,1,3.44619))
		constraintList.append( Sketcher.Constraint('DistanceX',1,1,2,1,8.44099))
		constraintList.append( Sketcher.Constraint('PointOnObject',12,1,0 ) )
		constraintList.append( Sketcher.Constraint('Vertical',12 ) )
		constraintList.append( Sketcher.Constraint('Coincident',13,1,12,2 ) )
		constraintList.append( Sketcher.Constraint('PointOnObject',13,2,3 ) )
		constraintList.append( Sketcher.Constraint('Vertical',13 ) )
		constraintList.append( Sketcher.Constraint('Equal',13,12))
		constraintList.append( Sketcher.Constraint('Coincident',12,2,-1,1 ) )
		
		
		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

		FreeCADGui.SendMsgToActiveView("ViewFit")	

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))

	def execute(self,fp):	
		fp.recompute()

FreeCADGui.addCommand('MicroUSB',MicroUSB_Command() ) 
