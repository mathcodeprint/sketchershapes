import Sketcher
import FreeCAD
import FreeCAD,FreeCADGui,Part, Draft
import os

__title__="FreeCAD Stick Framers Toolkit"
__author__ = "Paul Randall"
__url = "http://www.mathcodeprint.com/"
__command_name__ = "IBeamProfile" #Name of the command to appear in Toolbar
__command_group__= "" #Name of Toolbar to assign the command

class IBeamProfile_Command:
	def GetResources(slf):
		#print 'Run getResources() for IBeamProfile_Command' 
		image_path = '/sketchershapes/icons/ibeamprofile.png' 
		global_path = FreeCAD.getHomePath()+'Mod' 
		user_path = FreeCAD.getUserAppDataDir()+'Mod' 
		icon_path = '' 

		if os.path.exists(user_path + image_path): 
			icon_path = user_path + image_path 
		elif os.path.exists(global_path + image_path): 
			icon_path = global_path + image_path 
		return {'MenuText': 'IBeamProfile', 
			'ToolTip': 'Tooltip for IBeamProfile command', 
			'Pixmap' : str(icon_path) }  

	def IsActive(self): 
		if FreeCAD.ActiveDocument == None: 
			#print 'IBeamProfile command is NOT active' 
			return False 
		else: 
			#print 'IBeamProfile command IS active' 
			return True 
 
	def Activated(self): 
 
		#print 'IBeamProfileCommand activated' 
 
		b=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','IBeamProfile') 
		newsampleobject = IBeamProfile(b) 
		b.ViewObject.Proxy=0 
		FreeCAD.ActiveDocument.recompute()  
class IBeamProfile: 
 
 
	def __init__(self, obj):
 
		#print 'The IBeamProfile class has been instantiated init' 
		obj.Proxy = self
		App = FreeCAD
		tmpCircle = None
		geometryList = [] 
		constructionList = [] 
		constraintList = [] 
 
		geometryList.append( Part.LineSegment( App.Vector (8.330507757405432, 208.18772126896604, 0.0), App.Vector (84.53050775740543, 208.18772126896604, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (84.53050775740543, 208.18772126896604, 0.0), App.Vector (84.53050775740543, 201.83772126896605, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (84.53050775740543, 201.83772126896605, 0.0), App.Vector (52.14550775740543, 201.83772126896605, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (49.605507757405434, 199.29772126896606, 0.0), App.Vector (49.605507757405434, 13.87772126896607, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (52.14550775740543, 11.337721268966057, 0.0), App.Vector (84.53050775740543, 11.337721268966057, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (84.53050775740543, 11.337721268966057, 0.0), App.Vector (84.53050775740543, 4.987721268966057, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (84.53050775740543, 4.987721268966057, 0.0), App.Vector (8.330507757405428, 4.987721268966057, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (8.330507757405432, 4.987721268966057, 0.0), App.Vector (8.330507757405432, 11.337721268966057, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (8.330507757405432, 11.337721268966057, 0.0), App.Vector (40.71550775740543, 11.337721268966057, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (43.25550775740543, 13.877721268966058, 0.0), App.Vector (43.25550775740543, 199.29772126896606, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (40.71550775740543, 201.83772126896605, 0.0), App.Vector (8.330507757405428, 201.83772126896605, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (8.330507757405432, 201.83772126896605, 0.0), App.Vector (8.330507757405432, 208.18772126896604, 0.0)))
		tmpCircle = Part.Circle ( App.Vector (40.71550775740543, 199.29772126896606, 0.0), App.Vector (0.0, 0.0, 1.0), 2.54)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, -8.881784197001252e-16, 1.570796326794896))
		tmpCircle = Part.Circle ( App.Vector (52.14550775740543, 199.29772126896606, 0.0), App.Vector (0.0, 0.0, 1.0), 2.54)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 1.5707963267948977, 3.141592653589794))
		tmpCircle = Part.Circle ( App.Vector (40.71550775740543, 13.877721268966058, 0.0), App.Vector (0.0, 0.0, 1.0), 2.54)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 4.712388980384689, 6.283185307179586))
		tmpCircle = Part.Circle ( App.Vector (52.14550775740543, 13.877721268966058, 0.0), App.Vector (0.0, 0.0, 1.0), 2.54)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 3.141592653589793, 4.712388980384691))
		constructionList.append( Part.LineSegment( App.Vector (43.25550775740543, 13.877721268966058, 0.0), App.Vector (49.605507757405434, 13.877721268966058, 0.0)))


		constraintList.append( Sketcher.Constraint('Horizontal',0 ) )
		constraintList.append( Sketcher.Constraint('Coincident',0,2,1,1 ) )
		constraintList.append( Sketcher.Constraint('Vertical',1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',1,2,2,1 ) )
		constraintList.append( Sketcher.Constraint('Vertical',3 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',4 ) )
		constraintList.append( Sketcher.Constraint('Coincident',4,2,5,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',5,2,6,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',6,2,7,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',7,2,8,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',10,2,11,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',11,2,0,1 ) )
		constraintList.append( Sketcher.Constraint('Vertical',11 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',10 ) )
		constraintList.append( Sketcher.Constraint('Vertical',9 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',8 ) )
		constraintList.append( Sketcher.Constraint('Vertical',7 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',2 ) )
		constraintList.append( Sketcher.Constraint('Vertical',5 ) )
		constraintList.append( Sketcher.Constraint('Vertical',7,2,10,2 ) )
		constraintList.append( Sketcher.Constraint('Equal',2,10))
		constraintList.append( Sketcher.Constraint('Equal',5,1))
		constraintList.append( Sketcher.Constraint('DistanceY',1,2,1,1,6.35))
		constraintList.append( Sketcher.Constraint('Tangent',9,2,12,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',10,1,12,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',2,2,13,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',3,1,13,2 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',12,3,13,3 ) )
		constraintList.append( Sketcher.Constraint('Radius',12,2.54 ))
		constraintList.append( Sketcher.Constraint('Equal',12,13))
		constraintList.append( Sketcher.Constraint('Tangent',8,2,14,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',9,1,14,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',3,2,15,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',4,1,15,2 ) )
		constraintList.append( Sketcher.Constraint('Equal',14,15))
		constraintList.append( Sketcher.Constraint('Horizontal',14,3,15,3 ) )
		constraintList.append( Sketcher.Constraint('Coincident',16,1,9,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',16,2,3,2 ) )
		constraintList.append( Sketcher.Constraint('Equal',16,1))
		constraintList.append( Sketcher.Constraint('DistanceY',6,2,0,1,203.2))
		constraintList.append( Sketcher.Constraint('DistanceX',6,2,5,2,76.2))
		constraintList.append( Sketcher.Constraint('Equal',8,4))
		constraintList.append( Sketcher.Constraint('Horizontal',6 ) )
		constraintList.append( Sketcher.Constraint('Equal',13,15))


		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))
		#FreeCAD.Console.print.writeMessage('Changed property: ' + name + 'to ' + newvalue + '') 

	def execute(self,fp):	
		fp.recompute()
		print (' IBeamProfile Class executed() ') 
FreeCADGui.addCommand('IBeamProfile',IBeamProfile_Command() ) 
