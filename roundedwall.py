import Sketcher
import FreeCAD
import FreeCAD,FreeCADGui,Part, Draft
import os

__title__="FreeCAD Stick Framers Toolkit"
__author__ = "Paul Randall"
__url = "http://www.mathcodeprint.com/"
__command_name__ = "RoundedWall" #Name of the command to appear in Toolbar
__command_group__= "" #Name of Toolbar to assign the command

class RoundedWall_Command:
	def GetResources(slf):
		#print 'Run getResources() for RoundedWall_Command' 
		image_path = '/sketchershapes/icons/roundedwall.png' 
		global_path = FreeCAD.getHomePath()+'Mod' 
		user_path = FreeCAD.getUserAppDataDir()+'Mod' 
		icon_path = '' 

		if os.path.exists(user_path + image_path): 
			icon_path = user_path + image_path 
		elif os.path.exists(global_path + image_path): 
			icon_path = global_path + image_path 
		return {'MenuText': 'RoundedWall', 
			'ToolTip': 'Tooltip for RoundedWall command', 
			'Pixmap' : str(icon_path) }  

	def IsActive(self): 
		if FreeCAD.ActiveDocument == None: 
			#print 'RoundedWall command is NOT active' 
			return False 
		else: 
			#print 'RoundedWall command IS active' 
			return True 
 
	def Activated(self): 
 
		#print 'RoundedWallCommand activated' 
 
		b=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','RoundedWall') 
		newsampleobject = RoundedWall(b) 
		b.ViewObject.Proxy=0 
		FreeCAD.ActiveDocument.recompute()  
class RoundedWall: 
 
 
	def __init__(self, obj):
 
		#print 'The RoundedWall class has been instantiated init' 
		obj.Proxy = self
		App = FreeCAD
		tmpCircle = None
		geometryList = [] 
		constructionList = [] 
		constraintList = [] 
 
		geometryList.append( Part.LineSegment( App.Vector (-45.720540638031714, 100.43124811539978, 0.0), App.Vector (45.7205406380317, 100.43124811539978, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (70.72054063803168, 75.43124811539978, 0.0), App.Vector (70.72054063803168, -75.43124811539981, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (45.72054063803167, -100.4312481153998, 0.0), App.Vector (-45.720540638031714, -100.4312481153998, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-70.72054063803171, -75.4312481153998, 0.0), App.Vector (-70.72054063803171, 75.4312481153998, 0.0)))
		tmpCircle = Part.Circle ( App.Vector (-45.720540638031714, 75.43124811539978, 0.0), App.Vector (0.0, 0.0, 1.0), 25.0)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 1.5707963267948966, 3.141592653589793))
		tmpCircle = Part.Circle ( App.Vector (45.720540638031686, 75.43124811539978, 0.0), App.Vector (0.0, 0.0, 1.0), 25.0)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 0.0, 1.5707963267948963))
		tmpCircle = Part.Circle ( App.Vector (45.72054063803168, -75.4312481153998, 0.0), App.Vector (0.0, 0.0, 1.0), 25.0)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 4.71238898038469, 6.283185307179586))
		tmpCircle = Part.Circle ( App.Vector (-45.7205406380317, -75.4312481153998, 0.0), App.Vector (0.0, 0.0, 1.0), 25.0)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 3.141592653589793, 4.71238898038469))
		geometryList.append( Part.LineSegment( App.Vector (-45.720540638031714, 92.43124811539978, 0.0), App.Vector (45.720540638031686, 92.43124811539978, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (62.720540638031686, 75.43124811539978, 0.0), App.Vector (62.720540638031686, -75.43124811539981, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (45.72054063803168, -92.4312481153998, 0.0), App.Vector (-45.72054063803171, -92.4312481153998, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-62.72054063803169, -75.43124811539978, 0.0), App.Vector (-62.72054063803171, 75.43124811539981, 0.0)))
		tmpCircle = Part.Circle ( App.Vector (-45.720540638031714, 75.43124811539978, 0.0), App.Vector (0.0, 0.0, 1.0), 16.999999999999993)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 1.5707963267948963, 3.141592653589793))
		tmpCircle = Part.Circle ( App.Vector (45.720540638031686, 75.43124811539978, 0.0), App.Vector (0.0, 0.0, 1.0), 16.999999999999996)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 0.0, 1.5707963267948966))
		tmpCircle = Part.Circle ( App.Vector (45.72054063803168, -75.4312481153998, 0.0), App.Vector (0.0, 0.0, 1.0), 17.000000000000004)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 4.71238898038469, 6.283185307179586))
		tmpCircle = Part.Circle ( App.Vector (-45.7205406380317, -75.4312481153998, 0.0), App.Vector (0.0, 0.0, 1.0), 16.999999999999993)
		geometryList.append( Part.ArcOfCircle (  tmpCircle, 3.1415926535897927, 4.71238898038469))


		constraintList.append( Sketcher.Constraint('Horizontal',0 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',2 ) )
		constraintList.append( Sketcher.Constraint('Vertical',1 ) )
		constraintList.append( Sketcher.Constraint('Vertical',3 ) )
		constraintList.append( Sketcher.Constraint('Tangent',0,1,4,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',3,2,4,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',0,2,5,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',1,1,5,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',1,2,6,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',2,1,6,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',2,2,7,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',3,1,7,1 ) )
		constraintList.append( Sketcher.Constraint('Equal',4,5))
		constraintList.append( Sketcher.Constraint('Equal',5,6))
		constraintList.append( Sketcher.Constraint('Equal',6,7))
		constraintList.append( Sketcher.Constraint('Symmetric',5,3,7,3,-1))
		constraintList.append( Sketcher.Constraint('Horizontal',8 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',10 ) )
		constraintList.append( Sketcher.Constraint('Vertical',9 ) )
		constraintList.append( Sketcher.Constraint('Tangent',8,1,12,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',11,2,12,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',8,2,13,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',9,1,13,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',9,2,14,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',10,1,14,1 ) )
		constraintList.append( Sketcher.Constraint('Tangent',10,2,15,2 ) )
		constraintList.append( Sketcher.Constraint('Tangent',11,1,15,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',15,3,7,3 ) )
		constraintList.append( Sketcher.Constraint('Coincident',4,3,12,3 ) )
		constraintList.append( Sketcher.Constraint('Coincident',13,3,5,3 ) )
		constraintList.append( Sketcher.Constraint('Coincident',14,3,6,3 ) )
		constraintList.append( Sketcher.Constraint('Radius',5,25.0 ))
		constraintList.append( Sketcher.Constraint('DistanceY',2,1,10,1,8.0))


		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))
		#FreeCAD.Console.print.writeMessage('Changed property: ' + name + 'to ' + newvalue + '') 

	def execute(self,fp):	
		fp.recompute()
		print (' RoundedWall Class executed() ') 
FreeCADGui.addCommand('RoundedWall',RoundedWall_Command() ) 
