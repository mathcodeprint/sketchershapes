import Sketcher
import FreeCAD
import FreeCAD,FreeCADGui,Part
import os

class Parallelogram_Command:

	def GetResources(self):
		image_path = '/sketchershapes/icons/parallelogram.png' 
		global_path = FreeCAD.getHomePath()+'Mod' 
		user_path = FreeCAD.getUserAppDataDir()+'Mod' 
		icon_path = ""

		if os.path.exists(user_path + image_path): 
			icon_path = user_path + image_path 
		elif os.path.exists(global_path + image_path): 
			icon_path = global_path + image_path 
		return {"MenuText": "Parallelogram", 
			"ToolTip": "Create a sketch containing a parallelogram", 
			"Pixmap" : str(icon_path) }  

	def IsActive(self): 
		if FreeCAD.ActiveDocument == None: 
			#print 'Arrow command is NOT active' 
			return False 
		else: 
			#print 'Arrow command IS active' 
			return True 
 
	def Activated(self): 
 
		b=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','Parallelogram') 
		newsampleobject = Parallelogram(b) 
		b.ViewObject.Proxy=0 
		FreeCAD.ActiveDocument.recompute()  

class Parallelogram: 
 
	def __init__(self, obj): 
 
		obj.Proxy = self 

		App = FreeCAD

		geometryList = [] 
		constructionList = [] 
		constraintList = [] 
 
		geometryList.append( Part.LineSegment( App.Vector (-21.585465495875862, 31.505233569311592, 0.0), App.Vector (52.95370302174362, 32.7089267348908, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (52.95370302174362, 32.7089267348908, 0.0), App.Vector (12.331912517619486, -29.80032583442079, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (12.331912517619486, -29.80032583442079, 0.0), App.Vector (-62.207256, -31.004019, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (-62.207256, -31.004019, 0.0), App.Vector (-21.585465495875866, 31.505233569311592, 0.0)))
		constructionList.append( Part.LineSegment( App.Vector (-21.585465495875862, 31.505233569311592, 0.0), App.Vector (12.33191251761949, -29.800325834420782, 0.0)))
		constructionList.append( Part.LineSegment( App.Vector (-62.207256, -31.004019, 0.0), App.Vector (52.95370302174362, 32.7089267348908, 0.0)))


		constraintList.append( Sketcher.Constraint('Coincident',0,2,1,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',1,2,2,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',2,2,3,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',3,2,0,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',4,1,0,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',4,2,1,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',5,1,2,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',5,2,0,2 ) )

		constraintList.append( Sketcher.Constraint('Symmetric',2,2,0,2,4))
		constraintList.append( Sketcher.Constraint('Symmetric',0,1,1,2,5))

		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

		FreeCADGui.SendMsgToActiveView("ViewFit")	


	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))

	def execute(self,fp):	
		fp.recompute()

FreeCADGui.addCommand('Parallelogram', Parallelogram_Command() ) 
