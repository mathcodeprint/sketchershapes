import FreeCAD,FreeCADGui,Part, Sketcher
import os

class Holes_Command:

	def GetResources(self):
		print "Run getResources() for Holes_Command"
		image_path = '/sketchershapes/icons/holes.svg'
		global_path = FreeCAD.getHomePath()+"Mod"
		user_path = FreeCAD.getUserAppDataDir()+"Mod"
		icon_path = ""
		 
		if os.path.exists(user_path + image_path):
			icon_path = user_path + image_path
		elif os.path.exists(global_path + image_path):
			icon_path = global_path + image_path
		return {"MenuText": "Holes Menu",
			"ToolTip": "Tooltip for Holes command",
			'Pixmap' : str(icon_path) } 

	def IsActive(self):
		if FreeCAD.ActiveDocument == None:
			#print "Holes command is NOT active"
			return False
		else:
			#print "Holes command IS active"
			return True

	def Activated(self):
		print "Holes command activated"

		b=FreeCAD.ActiveDocument.addObject("Sketcher::SketchObjectPython","Holes")
		newholes = Holes(b)
		b.ViewObject.Proxy=0
		FreeCAD.ActiveDocument.recompute() 

class Holes:

	def __init__(self, obj):

		print "The Arrow class has been instantiated init"
	        obj.Proxy = self

		App = FreeCAD
		geometryList = []
		constructionList = []
		constraintList = []

### Begin Generated Code




### End Generated Code
		

		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))
		FreeCAD.Console.PrintMessage("Changed property: " + name + "to " + newvalue + "\n") 
	
	def execute(self,fp):	
		fp.recompute()
		print "Holes Class executed()"
			

FreeCADGui.addCommand('Holes', Holes_Command())

