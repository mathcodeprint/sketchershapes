import FreeCAD,FreeCADGui,Part, Sketcher
import os

class Holes_Command:

	def GetResources(self):
		image_path = '/sketchershapes/icons/holes.svg'
		global_path = FreeCAD.getHomePath()+"Mod"
		user_path = FreeCAD.getUserAppDataDir()+"Mod"
		icon_path = ""
		 
		if os.path.exists(user_path + image_path):
			icon_path = user_path + image_path
		elif os.path.exists(global_path + image_path):
			icon_path = global_path + image_path
		return {"MenuText": "Holes",
			"ToolTip": "Adds four equal circles for padding/pocketing",
			'Pixmap' : str(icon_path) } 

	def IsActive(self):
		if FreeCAD.ActiveDocument == None:
			#print "Holes command is NOT active"
			return False
		else:
			#print "Holes command IS active"
			return True

	def Activated(self):
		b=FreeCAD.ActiveDocument.addObject("Sketcher::SketchObjectPython","Holes")
		newholes = Holes(b)
		b.ViewObject.Proxy=0
		FreeCAD.ActiveDocument.recompute() 

class Holes:

	def __init__(self, obj):

		obj.Proxy = self

		App = FreeCAD
		geometryList = []
		constructionList = []
		constraintList = []

		constructionList.append( Part.LineSegment( App.Vector (-48.275214278323254, 13.174509824169139, 0.0), App.Vector (-10.625948329097524, 50.344735312620585, 0.0)))
		constructionList.append( Part.LineSegment( App.Vector (-10.625948329097524, 50.344735312620585, 0.0), App.Vector (23.11723396788206, 16.16667942036088, 0.0)))
		constructionList.append( Part.LineSegment( App.Vector (23.117233967882058, 16.16667942036088, 0.0), App.Vector (-14.532031981343673, -21.003546068090568, 0.0)))
		constructionList.append( Part.LineSegment( App.Vector (-14.532031981343673, -21.003546068090564, 0.0), App.Vector (-48.275214278323254, 13.174509824169139, 0.0)))
		geometryList.append( Part.Circle ( App.Vector (-10.625948329097524, 50.344735312620585, 0.0), App.Vector (0.0, 0.0, 1.0), 5.0))
		geometryList.append( Part.Circle ( App.Vector (23.117233967882058, 16.16667942036088, 0.0), App.Vector (0.0, 0.0, 1.0), 5.0))
		geometryList.append( Part.Circle ( App.Vector (-14.532031981343673, -21.003546068090564, 0.0), App.Vector (0.0, 0.0, 1.0), 5.0))
		geometryList.append( Part.Circle ( App.Vector (-48.275214278323254, 13.174509824169139, 0.0), App.Vector (0.0, 0.0, 1.0), 5.0))

		constraintList.append( Sketcher.Constraint('Coincident',5,1,0,3 ) )
		constraintList.append( Sketcher.Constraint('Equal',0,2))
		constraintList.append( Sketcher.Constraint('Equal',3,1))
		constraintList.append( Sketcher.Constraint('Radius',4,5.0 ))
		constraintList.append( Sketcher.Constraint('Coincident',0,3,4,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',6,1,1,3 ) )
		constraintList.append( Sketcher.Constraint('Coincident',6,1,5,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',6,2,7,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',2,3,6,2 ) )
		constraintList.append( Sketcher.Constraint('Coincident',7,2,3,3 ) )
		constraintList.append( Sketcher.Constraint('Coincident',4,1,3,3 ) )
		constraintList.append( Sketcher.Constraint('Perpendicular',4,0,7 ) )
		constraintList.append( Sketcher.Constraint('Equal',4,6))
		constraintList.append( Sketcher.Constraint('Equal',4,6))
		constraintList.append( Sketcher.Constraint('Equal',7,5))
		constraintList.append( Sketcher.Constraint('Equal',7,5))
		constraintList.append( Sketcher.Constraint('Equal',0,1))
		constraintList.append( Sketcher.Constraint('Radius',3,2.5 ))

		constraintList.append( Sketcher.Constraint('Distance',3,3,0,3,15 ))

		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

		FreeCADGui.SendMsgToActiveView("ViewFit")	

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))
	
	def execute(self,fp):	
		fp.recompute()
			

FreeCADGui.addCommand('Holes', Holes_Command())

