import Sketcher
import FreeCAD
import FreeCAD,FreeCADGui,Part, Draft
import os

__title__="FreeCAD Stick Framers Toolkit"
__author__ = "Paul Randall"
__url = "http://www.mathcodeprint.com/"
__command_name__ = "FourSquare" #Name of the command to appear in Toolbar
__command_group__= "" #Name of Toolbar to assign the command

class FourSquare_Command:
	def GetResources(slf):
		#print 'Run getResources() for FourSquare_Command' 
		image_path = '/sketchershapes/icons/foursquare.png' 
		global_path = FreeCAD.getHomePath()+'Mod' 
		user_path = FreeCAD.getUserAppDataDir()+'Mod' 
		icon_path = '' 

		if os.path.exists(user_path + image_path): 
			icon_path = user_path + image_path 
		elif os.path.exists(global_path + image_path): 
			icon_path = global_path + image_path 
		return {'MenuText': 'FourSquare', 
			'ToolTip': 'Tooltip for FourSquare command', 
			'Pixmap' : str(icon_path) }  

	def IsActive(self): 
		if FreeCAD.ActiveDocument == None: 
			#print 'FourSquare command is NOT active' 
			return False 
		else: 
			#print 'FourSquare command IS active' 
			return True 
 
	def Activated(self): 
 
		#print 'FourSquareCommand activated' 
 
		b=FreeCAD.ActiveDocument.addObject('Sketcher::SketchObjectPython','FourSquare') 
		newsampleobject = FourSquare(b) 
		b.ViewObject.Proxy=0 
		FreeCAD.ActiveDocument.recompute()  
class FourSquare: 
 
 
	def __init__(self, obj):
 
		#print 'The FourSquare class has been instantiated init' 
		obj.Proxy = self
		App = FreeCAD
		tmpCircle = None
		geometryList = [] 
		constructionList = [] 
		constraintList = [] 
 
		geometryList.append( Part.LineSegment( App.Vector (1.894559, 1.1174780000000002, 0.0), App.Vector (5.317714, 1.1174780000000002, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (5.317714, 1.1174780000000002, 0.0), App.Vector (5.317714, 4.764578, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (5.317714, 4.764578, 0.0), App.Vector (1.894559, 4.764578, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (1.894559, 4.764578, 0.0), App.Vector (1.894559, 1.1174780000000002, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (6.1815, 5.436411, 0.0), App.Vector (9.316726, 5.436411, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (9.316726, 5.436411, 0.0), App.Vector (9.316726, 8.539645, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (9.316726, 8.539645, 0.0), App.Vector (6.1815, 8.539645, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (6.1815, 8.539645, 0.0), App.Vector (6.1815, 5.436411, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (6.34146, 4.540632, 0.0), App.Vector (9.316726, 4.540632, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (9.316726, 4.540632, 0.0), App.Vector (9.316726, 1.373415, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (9.316726, 1.373415, 0.0), App.Vector (6.34146, 1.373415, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (6.34146, 1.373415, 0.0), App.Vector (6.34146, 4.540632, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (1.830574, 8.379684, 0.0), App.Vector (5.12576, 8.379684, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (5.12576, 8.379684, 0.0), App.Vector (5.12576, 5.72434, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (5.12576, 5.72434, 0.0), App.Vector (1.830574, 5.72434, 0.0)))
		geometryList.append( Part.LineSegment( App.Vector (1.830574, 5.72434, 0.0), App.Vector (1.830574, 8.379684, 0.0)))


		constraintList.append( Sketcher.Constraint('Coincident',0,2,1,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',1,2,2,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',2,2,3,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',3,2,0,1 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',0 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',2 ) )
		constraintList.append( Sketcher.Constraint('Vertical',1 ) )
		constraintList.append( Sketcher.Constraint('Vertical',3 ) )
		constraintList.append( Sketcher.Constraint('Coincident',4,2,5,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',5,2,6,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',6,2,7,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',7,2,4,1 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',4 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',6 ) )
		constraintList.append( Sketcher.Constraint('Vertical',5 ) )
		constraintList.append( Sketcher.Constraint('Vertical',7 ) )
		constraintList.append( Sketcher.Constraint('Coincident',8,2,9,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',9,2,10,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',10,2,11,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',11,2,8,1 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',8 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',10 ) )
		constraintList.append( Sketcher.Constraint('Vertical',9 ) )
		constraintList.append( Sketcher.Constraint('Vertical',11 ) )
		constraintList.append( Sketcher.Constraint('Coincident',12,2,13,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',13,2,14,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',14,2,15,1 ) )
		constraintList.append( Sketcher.Constraint('Coincident',15,2,12,1 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',12 ) )
		constraintList.append( Sketcher.Constraint('Horizontal',14 ) )
		constraintList.append( Sketcher.Constraint('Vertical',13 ) )
		constraintList.append( Sketcher.Constraint('Vertical',15 ) )


		obj.addGeometry( geometryList, False)
		obj.addGeometry( constructionList, True)
		obj.addConstraint( constraintList )

	def onChanged(self, fp, prop):
		name = str(prop)
		newvalue = str(fp.getPropertyByName(str(prop)))
		#FreeCAD.Console.print.writeMessage('Changed property: ' + name + 'to ' + newvalue + '') 

	def execute(self,fp):	
		fp.recompute()
		print (' FourSquare Class executed() ') 
FreeCADGui.addCommand('FourSquare',FourSquare_Command() ) 
